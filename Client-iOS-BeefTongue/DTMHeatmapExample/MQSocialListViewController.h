//
//  MQSocialListViewController.h
//  mahoq
//
//  Created by tana on 2014/08/31.
//  Copyright (c) 2014年 tana. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MQSocialDataManager.h"
#import "STTweetCell.h"

@interface MQSocialListViewController : UITableViewController
    <UINavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate>
{
    
    MQSocialDataManager *socialDataManager;
    
    NSString *searchKeyword;
    NSString *activeLocale;
    
    /**
     * This cell is just used to calculate height for row. Not display.
     */
    __strong STTweetCell *_cellForHeight;

}

@property (nonatomic, retain) NSString *searchKeyword;
@property (nonatomic, retain) NSString *activeLocale;

@end

