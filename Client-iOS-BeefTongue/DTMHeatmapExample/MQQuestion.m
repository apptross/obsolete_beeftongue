//////////////////////////////////////////////////////
//
//  Copyright 2012 Yuki Tanaike. All rights reserved.
//
//////////////////////////////////////////////////////

#import "MQQuestion.h"

@implementation MQQuestion
@synthesize qid;
@synthesize title;
@synthesize url;
@synthesize detail;
@synthesize category;
@synthesize pub_date;
@synthesize comment;
@synthesize ans_date;

////////////////////////////
//for saving

- (void)encodeWithCoder:(NSCoder*)coder {
	[coder encodeObject:qid      forKey:@"MQ_QID"];
	[coder encodeObject:title    forKey:@"MQ_TITLE"];
	[coder encodeObject:url      forKey:@"MQ_URL"];
	[coder encodeObject:detail   forKey:@"MQ_DETAIL"];
	[coder encodeObject:category forKey:@"MQ_CATEGORY"];
	[coder encodeObject:pub_date forKey:@"MQ_PUBDATE"];
	[coder encodeObject:comment  forKey:@"MQ_COMMENT"];
	[coder encodeObject:ans_date forKey:@"MQ_ANSDATE"];
}

- (id)initWithCoder:(NSCoder*)coder {
	qid      = [coder decodeObjectForKey:@"MQ_QID"];
	title    = [coder decodeObjectForKey:@"MQ_TITLE"];
	url      = [coder decodeObjectForKey:@"MQ_URL"];
	detail   = [coder decodeObjectForKey:@"MQ_DETAIL"];
	category = [coder decodeObjectForKey:@"MQ_CATEGORY"];
	pub_date = [coder decodeObjectForKey:@"MQ_PUBDATE"];
	comment  = [coder decodeObjectForKey:@"MQ_COMMENT"];
	ans_date = [coder decodeObjectForKey:@"MQ_ANSDATE"];

	return self;
}

////////////////////////////
//for sorting
- (NSString *)description {
    return [NSString stringWithFormat:@"%@ - %@",
            [super description], pub_date];
}

@end
