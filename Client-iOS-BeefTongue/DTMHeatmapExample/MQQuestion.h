//////////////////////////////////////////////////////
//
//  Copyright 2012 Yuki Tanaike. All rights reserved.
//
//////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

@interface MQQuestion : NSObject  <NSCoding> {

    NSString *qid;
    NSString *title;
    NSString *detail;
    NSString *url;
    NSDate   *pub_date;
    NSString *category;
    NSString *comment;
    NSDate   *ans_date;

}

@property (nonatomic, copy) NSString *qid;        //required
@property (nonatomic, copy) NSString *title;      //required
@property (nonatomic, copy) NSString *detail;     //required
@property (nonatomic, copy) NSString *url;        //required
@property (nonatomic, copy) NSDate   *pub_date;
@property (nonatomic, copy) NSString *category;
@property (nonatomic, copy) NSString *comment;
@property (nonatomic, copy) NSDate   *ans_date; //the date which a user  ansered last

@end

