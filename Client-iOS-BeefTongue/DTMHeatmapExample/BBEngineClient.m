//
//  BBEngineClient.m
//  BookButler_iPhone
//
//  Created by tana on 2015/06/22.
//  Copyright (c) 2015 Yuki.Tanaike. All rights reserved.
//

#import <CocoaLumberjack/DDLog.h>

#import "BBEngineClient.h"

//#import "BBEngineJSONResponseSerializer.h"
//#import "NSError+BB.h"

#import "DebugLogConfig.h"

//#define BASE_HOST_DEFAULT @"http://localhost:8888/template_testdata/"
#define BASE_HOST_DEFAULT @"http://sandbox.apptross.com/engine/beeftongue/"
#define TIME_INTERVAL_SEC               10.0
//#define LOAD_LIST_PATH	@"List.php"
#define LOAD_LIST_PATH	@"HeatList"

@implementation BBEngineClient {
//    NSString *mUDID;
}

+ (instancetype)sharedInstance
{
    static BBEngineClient *_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    
    return _instance;
}

- (instancetype)init
{
    // NSURLSessionConfiguration も指定したい場合は
    // initWithBaseURL:sessionConfiguration: を使う
    // 未指定の場合 [NSURLSessionConfiguration defaultSessionConfiguration] が使われる。
    if (self = [super initWithBaseURL:[NSURL URLWithString:BASE_HOST_DEFAULT]]) {
        
        // Default requestSerializer is AFHTTPRequestSerializer
        
        // Use our custom response serializer (Default responseSerializer is AFJSONResponseSerializer)
//        self.responseSerializer = [BBEngineJSONResponseSerializer serializer];
        
        // 現在はBodyをJson形式で受け取っているが、ContentTypesは"text/plain"で受け取っている。本来は"application/json"形式で受け取るべき。
        self.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
        
        // set udid
//         mUDID = [LocalConfiguration loadSecureUDID];
    }
    return self;
}

- (NSURLSessionDataTask *)dataTaskWithRequest:(NSURLRequest *)request
                            completionHandler:(void (^)(NSURLResponse *, id, NSError *))completionHandler
{
    NSMutableURLRequest *mutableRequest = [request mutableCopy];
    
    [mutableRequest setCachePolicy:NSURLRequestUseProtocolCachePolicy];
    [mutableRequest setTimeoutInterval:TIME_INTERVAL_SEC];
//    [mutableRequest setValue:USER_AGENT_STR forHTTPHeaderField:@"User-Agent"];
    
    return [super dataTaskWithRequest:mutableRequest completionHandler:completionHandler];
}


- (void)commonRequest:(void (^)(NSDictionary *, NSError *))handler
              subPath:(NSString *)subpath
                param:(NSDictionary *)params
       availableRetry:(BOOL)availableRetry
{
    DDLogDebug(@"Client Request : %@", subpath);
    
    [self GET:subpath parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if (handler) {
            DDLogDebug(@"responseObject : %@", responseObject);
            handler(responseObject, nil);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (handler) {
            
            DDLogDebug(@"error : %@", error);

            handler(nil, error);
            
        }
    }];
    return ;
}

- (void)loadHeatList:(NSString *)category
            language:(NSString *)language
             handler:(void (^)(NSDictionary *, NSError *))handler
{
    
    NSDictionary* params = @{
                             @"category" : category,
                             @"language" : language
                             };
    [self commonRequest:handler subPath:LOAD_LIST_PATH param:params availableRetry:YES];
}

@end
