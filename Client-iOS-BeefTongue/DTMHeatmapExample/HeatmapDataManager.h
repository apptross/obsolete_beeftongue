//
//  HeatmapDataManager.h
//  DTMHeatmapExample
//
//  Created by tana on 2016/06/04.
//  Copyright © 2016年 Bryan Oltman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HeatmapDataManager : NSObject {
    NSMutableArray *mHeatlist;
}

@property (nonatomic, retain) NSMutableArray* mHeatlist;

+ (instancetype)sharedInstance;

- (void)loadHeatList:(NSString *)category
            language:(NSString *)language
             handler:(void (^)(NSDictionary *, NSError *))handler;

@end
