//
//  HeatmapDataManager.m
//  DTMHeatmapExample
//
//  Created by tana on 2016/06/04.
//  Copyright © 2016年 Bryan Oltman. All rights reserved.
//

#import "HeatmapDataManager.h"
#import "BBEngineClient.h"

#import "DebugLogConfig.h"

#import "BTHeatData.h"

@implementation HeatmapDataManager {
    BBEngineClient *mBBClient;
}

@synthesize mHeatlist;

+ (instancetype)sharedInstance {
    static HeatmapDataManager* _instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    return _instance;
}

-(id)init {
    self = [super init];
    if(self != nil) {
        mHeatlist = [[NSMutableArray alloc] initWithCapacity:1];
        mBBClient = [BBEngineClient sharedInstance];
    }
    return self;
}

- (void)loadHeatList:(NSString *)category
            language:(NSString *)language
             handler:(void (^)(NSDictionary *, NSError *))handler {
    
    [mBBClient loadHeatList:category
                   language:language
                    handler:^(NSDictionary *result, NSError *error) {
            if (error == nil) {
                // success
                mHeatlist = [self getHeatListByHeatDataDic:result];
                handler(result, nil);
                
            } else {
                // failer
                handler(nil, error);
            }
        }];
    
}

- (void) clearHeatData{
    [mHeatlist removeAllObjects];
}

- (NSMutableArray *)getHeatListByHeatDataDic:(NSDictionary* )heatDic
{
    
    DDLogDebug(@"jsonDic : %@", [heatDic description]);
    DDLogDebug(@"status : %@", [heatDic objectForKey:@"status"]);
    
    NSArray *tmpHeatList = [NSArray arrayWithArray:[heatDic objectForKey:@"landmarks"]];
    
    NSMutableArray *retHeatList = [[NSMutableArray alloc] initWithCapacity:1];
    
    for (NSDictionary *heatDic in tmpHeatList) {
        
        BTHeatData *heatdata = [self convertHeatMapDictionaryToObject:heatDic];
        [retHeatList addObject: heatdata];
        
    }
    
    return retHeatList;
}


- (BTHeatData *)convertHeatMapDictionaryToObject:(NSDictionary * )bookDictionary
{
    
    BTHeatData *curHeatData = [[BTHeatData alloc] init];
    
    DDLogDebug(@"convertHeatMapDictionaryToObject started");
    curHeatData.hmid = [bookDictionary objectForKey:@"id"];
    curHeatData.name = [bookDictionary objectForKey:@"name"];
    curHeatData.lat = [bookDictionary objectForKey:@"latitude"];
    curHeatData.lng = [bookDictionary objectForKey:@"longitude"];
    curHeatData.heat = [[bookDictionary objectForKey:@"heat"] doubleValue];
    
    return curHeatData;
}



@end
