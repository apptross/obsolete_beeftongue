//
//  Util.m
//  BookButler_iPhone
//
//  Created by Tanaike on 2013/03/08.
//  Copyright 2013 Yuki Tanaike. All rights reserved.
//

#import "Util.h"
#import "SVProgressHUD.h"

@implementation Util

+ (void) dismissSVProgressHUD:(NSString *)successMsg errorMsg:(NSString *)errorMsg
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // time-consuming task
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (errorMsg) {
                [SVProgressHUD showErrorWithStatus:errorMsg];
            } else if (successMsg) {
                [SVProgressHUD showSuccessWithStatus:successMsg];
            } else {
                [SVProgressHUD dismiss];
            }
            
        });
    });
    
}


+ (void) dismissSVProgressHUDWithErrorMessage:(NSString *)errorMsg
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // time-consuming task
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (errorMsg) {
                [SVProgressHUD showErrorWithStatus:errorMsg];
            } else {
                [SVProgressHUD dismiss];
            }
            
        });
    });
    
}


+ (void) dismissSVProgressHUDWithSuccessMessage:(NSString *)successMsg
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // time-consuming task
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (successMsg) {
                [SVProgressHUD showSuccessWithStatus:successMsg];
            } else {
                [SVProgressHUD dismiss];
            }
            
        });
    });
    
}

@end
