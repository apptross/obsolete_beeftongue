//
//  MQSocialListViewController.m
//  mahoq
//
//  Created by tana on 2014/08/31.
//  Copyright (c) 2014年 tana. All rights reserved.
//

#import "MQSocialListViewController.h"

#import "SVProgressHUD.h"

#import "STTweetRowData.h"
#import "STTweetCell.h"
#import "STTableViewCell.h"

#import "STCell-Custom-Def.h"

@interface MQSocialListViewController ()

@end


@implementation MQSocialListViewController

@synthesize searchKeyword;
@synthesize activeLocale;


- (void)onRefresh:(id)sender
{
    [self.refreshControl beginRefreshing];
    
    NSLog(@"refresh start");
    
    [socialDataManager getTimeline:searchKeyword locale:activeLocale delegate:self];

    [self.refreshControl endRefreshing];
}

- ( void )onTap:( id )inSender
{
    NSLog(@"onTap");
    [self dismissModalViewControllerAnimated:YES];
    return;
}

- (void)viewDidLoad
{

    [super viewDidLoad];
////    self.navigationController.delegate = self;
//    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 44, self.view.bounds.size.width, 44)];
//    UIBarButtonItem * btn = [[UIBarButtonItem alloc] initWithTitle:@"テスト" style:UIBarButtonItemStyleBordered target:self action:@selector(onTap:)];
//    toolBar.items = [ NSArray arrayWithObjects:btn, nil ];
//    
//    toolBar.barTintColor =  [UIColor colorWithRed:0.077 green:0.411 blue:0.672 alpha:1.000];
//    [self.view addSubview:toolBar];
    
    
    UIButton *btnTmp = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btnTmp setFrame:CGRectMake(0,-40,100,40)];
    [btnTmp setTitle:@"Done" forState:UIControlStateNormal];
    [btnTmp setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btnTmp setTitleColor:[UIColor redColor] forState:UIControlStateHighlighted];
    [btnTmp addTarget:self
            action:@selector(onTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnTmp];

    UIEdgeInsets insets = self.tableView.contentInset;
    insets.top += 60;
    self.tableView.contentInset = insets;
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(onRefresh:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;

    
    _cellForHeight = [[STTweetCell alloc] initWithFrame:CGRectZero];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
//    self.navigationItem.title = @"みんなの答え";
    
    socialDataManager = [[MQSocialDataManager alloc] init];
    [socialDataManager initialize];
    [socialDataManager getTimeline:searchKeyword locale:activeLocale delegate:self];

}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    //一番下までスクロールしたかどうか
    if(self.tableView.contentOffset.y >= (self.tableView.contentSize.height
                                          - self.tableView.bounds.size.height))
    {
        //表示件数を追加する
        NSLog(@"add question which is shown in table view");
        
        [socialDataManager moveInvisTweetsToVisTweets];
        
        //update table
        [self refreshTableOnFront];
        
    }
    
}


//フロント側でテーブルを更新
- (void) refreshTableOnFront {
    [self performSelectorOnMainThread:@selector(refreshTable) withObject:self waitUntilDone:TRUE];
}

//テーブルの内容をセット
- (void)refreshTable {
    
    //ステータスバーのActivity Indicatorを停止
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    //最新の内容にテーブルをセット
	[self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSLog(@"visTweets.count %lu", (unsigned long)socialDataManager.visTweets.count);
    
//    return visTweets.count;
    return socialDataManager.visTweets.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *tweetMessage = [socialDataManager.visTweets objectAtIndex:[indexPath row]];
    STTweetRowData *tweetRowDara = [socialDataManager setTweetRowDataByTweetDic:tweetMessage];
    
    NSString *cellId = @"Cell_SOCIAL";
    STTweetCell *cell = (STTweetCell *)[self.tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[STTweetCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    
    [cell setupRowData:tweetRowDara];
    
    return cell;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"tableView:heightForRowAtIndexPath indexPath.row:%ld", (long)indexPath.row);

    NSDictionary *tweetMessage = [socialDataManager.visTweets objectAtIndex:[indexPath row]];
    STTweetRowData *tweetRowDara = [socialDataManager setTweetRowDataByTweetDic:tweetMessage];

    [_cellForHeight setupRowData:tweetRowDara];
    
    CGSize size;
    size.width = self.tableView.frame.size.width;
    size.height = STMaxCellHeight;
    size = [_cellForHeight sizeThatFits:size];
    return size.height;
}


@end
