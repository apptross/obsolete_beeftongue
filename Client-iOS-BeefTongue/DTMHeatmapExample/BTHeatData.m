//
//  BTHeapMapHea.m
//  DTMHeatmapExample
//
//  Created by tana on 2016/06/04.
//  Copyright © 2016年 Bryan Oltman. All rights reserved.
//

#import "BTHeatData.h"

@implementation BTHeatData

@synthesize hmid;
@synthesize name;
@synthesize lat;
@synthesize lng;
@synthesize heat;

@end
