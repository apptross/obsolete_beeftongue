//
//  STCell-Custom-Def.h
//  DTMHeatmapExample
//
//  Created by tana on 2016/06/05.
//  Copyright © 2016年 Bryan Oltman. All rights reserved.
//

#ifndef STCell_Custom_Def_h
#define STCell_Custom_Def_h


//for SFFlexibleCellHeight
#define STMargin 8
#define STUserIconSize 48
#define STMaxCellHeight 2000

#endif /* STCell_Custom_Def_h */
