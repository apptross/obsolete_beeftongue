//
//  BTHeapMapHea.h
//  DTMHeatmapExample
//
//  Created by tana on 2016/06/04.
//  Copyright © 2016年 Bryan Oltman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTHeatData : NSObject

@property (nonatomic, copy) NSString *hmid;
@property (nonatomic, copy) NSString *name;
//@property (nonatomic, copy) NSString *eng_name;
@property (nonatomic, copy) NSString *lat;
@property (nonatomic, copy) NSString *lng;

@property (nonatomic, assign) double heat;
//@property (nonatomic, copy) NSString *language;

@end
