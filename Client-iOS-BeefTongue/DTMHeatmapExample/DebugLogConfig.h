//
//  DebugConfig.h
//  BookButler_iPhone
//
//  Created by tanaike on 2015/04/30.
//  Copyright (c) 2015 tanaike. All rights reserved.
//

#ifndef BookButler_iPhone_DebugLogConfig
#define BookButler_iPhone_DebugLogConfig

#import <CocoaLumberjack/DDLog.h>
#import "DDLog.h"
#import "DDTTYLogger.h"
#import "DDASLLogger.h"

#if defined(DEBUG) || defined(ADHOC)  // Debug or Adhoc setting

#define METHOD_LOG DDLogDebug(@"[Class Method]: %s (Line:%d)", __func__, __LINE__);

// DDLog setting : デバッグ時は全レベルのログを表示する
static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#define SETUP_LOGGER( args... ) [DDLog addLogger:[DDTTYLogger sharedInstance]]; \
[DDLog addLogger:[DDASLLogger sharedInstance]];


#else // Release setting

#define METHOD_LOG

// DDLog setting : リリース時はログを表示しない
static const int ddLogLevel = LOG_LEVEL_OFF;

#define SETUP_LOGGER( args... )

//ログ出力コードはリリース時には削除すべきなので，Defineで切り替える (NSLogのコードを削除する)
//ref : http://iphone-dev.g.hatena.ne.jp/tokorom/20100421/1271861868
#if !defined(NS_BLOCK_ASSERTIONS)
#if !defined(NSLog)
#define NSLog( args... ) NSLog( args, 0 )
#endif /* NSLog */
#else /* NS_BLOCK_ASSERTIONS */
#if !defined(NSLog)
#define NSLog( args... )
#endif /* NSLog */
#endif  /* NS_BLOCK_ASSERTIONS */

#endif /* DEBUG */


#endif /* BookButler_iPhone_DebugLogConfig */

