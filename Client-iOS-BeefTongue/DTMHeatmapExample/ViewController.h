//
//  ViewController.h
//  DTMHeatMapExample
//
//  Created by Bryan Oltman on 1/7/15.
//  Copyright (c) 2015 Dataminr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "SlideNavigationController.h"

/*
 <SlideNavigationControllerDelegate>
 */
@interface ViewController : UIViewController <MKMapViewDelegate, SlideNavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
// 未使用
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barBtn;
//@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

- (IBAction)onLeftBtnPush:(id)sender;

@end

