//
//  ViewController.m
//  DTMHeatMapExample
//
//  Created by Bryan Oltman on 1/7/15.
//  Copyright (c) 2015 Dataminr. All rights reserved.
//

#import "ViewController.h"
#import "DTMHeatmapRenderer.h"
#import "DTMDiffHeatmap.h"
#import "HeatmapDataManager.h"

#import "SVProgressHUD.h"
#import "Util.h"

#import "BTHeatData.h"

#import "SlideNavigationController.h"

#import "ActionSheetPicker.h"
#import "MQSocialListViewController.h"

#import "MQSocialDataManager.h"

@interface ViewController () {
    HeatmapDataManager  *mHeatmapDataManager;
    NSString *mActiveCategory;
    NSString *mActiveLanguage;
    
    BOOL isMarkerVisible;
}
@property (strong, nonatomic) DTMHeatmap *heatmap;
//@property (strong, nonatomic) DTMDiffHeatmap *diffHeatmap;
@end

@implementation ViewController

- (void)viewDidLoad
{
    isMarkerVisible = false;
    
    [super viewDidLoad];
    [self setupHeatmaps];
    
}

- (void)setupHeatmaps
{
    // Set map region
    MKCoordinateSpan span = MKCoordinateSpanMake(15.0, 15.0);
//    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(38.5556, -121.4689);
//    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(35.425, 136.99);
//    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(36.39111, 139.06083);
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(38.24056, 140.36333);
    self.mapView.region = MKCoordinateRegionMake(center, span);
    
    self.heatmap = [DTMHeatmap new];
    [self.heatmap setData:[self parseLatLonFile:@"mcdonalds"]];
    [self.mapView addOverlay:self.heatmap];

//    self.diffHeatmap = [DTMDiffHeatmap new];
//    [self.diffHeatmap setBeforeData:[self parseLatLonFile:@"first_week"]
//                          afterData:[self parseLatLonFile:@"third_week"]];
    
    mHeatmapDataManager = [HeatmapDataManager sharedInstance];
    
    self.titleLabel.text = @"hotspa";
//    titleLabel.titleLabel = @"test";
    mActiveCategory = @"hotspa";
    mActiveLanguage = @"ja";
    [self reloadHeagmap];
    
}

- (void) reloadHeagmap
{
#if 1
    NSString *category = mActiveCategory;
    NSString *language = mActiveLanguage;
    [SVProgressHUD show];
    [mHeatmapDataManager loadHeatList:category
                             language:language
                              handler:^(NSDictionary *result, NSError *error) {
                                  [Util dismissSVProgressHUD:nil errorMsg:nil];
                                  
                                  // 再描画
                                  [self.mapView removeOverlay:self.heatmap];
                                  
                                  // todo : 結果をメンバ変数ではなく、引数で受け取れるようにするべき
                                  [self.heatmap setData:[self convert2DtmData:mHeatmapDataManager.mHeatlist]];
                                  [self.mapView addOverlay:self.heatmap];
                                  
                              }];
#endif
    
}

- (NSDictionary *)convert2DtmData:(NSMutableArray *)myHeatlist
{
    NSMutableDictionary *ret = [NSMutableDictionary new];
    
    for (BTHeatData* myHeatData in myHeatlist) {
        
        NSString *latStr = myHeatData.lat;
        NSString *lonStr = myHeatData.lng;
        
        CLLocationDegrees latitude = [latStr doubleValue];
        CLLocationDegrees longitude = [lonStr doubleValue];
        
        // For this example, each location is weighted equally
//        double weight = 1;
        double weight = myHeatData.heat;
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude
                                                          longitude:longitude];
        MKMapPoint point = MKMapPointForCoordinate(location.coordinate);
        NSValue *pointValue = [NSValue value:&point
                                withObjCType:@encode(MKMapPoint)];
        ret[pointValue] = @(weight);
        
        
    }
    
//    NSMutableDictionary *ret = [NSMutableDictionary new];
//    NSString *path = [[NSBundle mainBundle] pathForResource:fileName
//                                                     ofType:@"txt"];
//    NSString *content = [NSString stringWithContentsOfFile:path
//                                                  encoding:NSUTF8StringEncoding
//                                                     error:NULL];
//    NSArray *lines = [content componentsSeparatedByString:@"\n"];
//    for (NSString *line in lines) {
//        NSArray *parts = [line componentsSeparatedByString:@","];
//        NSString *latStr = parts[0];
//        NSString *lonStr = parts[1];
//        
//        CLLocationDegrees latitude = [latStr doubleValue];
//        CLLocationDegrees longitude = [lonStr doubleValue];
//        
//        // For this example, each location is weighted equally
//        double weight = 1;
//        
//        CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude
//                                                          longitude:longitude];
//        MKMapPoint point = MKMapPointForCoordinate(location.coordinate);
//        NSValue *pointValue = [NSValue value:&point
//                                withObjCType:@encode(MKMapPoint)];
//        ret[pointValue] = @(weight);
//    }
//    
    return ret;
}

- (NSDictionary *)parseLatLonFile:(NSString *)fileName
{
    NSMutableDictionary *ret = [NSMutableDictionary new];
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName
                                                     ofType:@"txt"];
    NSString *content = [NSString stringWithContentsOfFile:path
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    NSArray *lines = [content componentsSeparatedByString:@"\n"];
    for (NSString *line in lines) {
        NSArray *parts = [line componentsSeparatedByString:@","];
        NSString *latStr = parts[0];
        NSString *lonStr = parts[1];
        
        CLLocationDegrees latitude = [latStr doubleValue];
        CLLocationDegrees longitude = [lonStr doubleValue];
        
        // For this example, each location is weighted equally
        double weight = 1;
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude
                                                          longitude:longitude];
        MKMapPoint point = MKMapPointForCoordinate(location.coordinate);
        NSValue *pointValue = [NSValue value:&point
                                withObjCType:@encode(MKMapPoint)];
        ret[pointValue] = @(weight);
    }
    
    return ret;
}

//
//- (IBAction)segmentedControlValueChanged:(UISegmentedControl *)sender
//{
//    switch (sender.selectedSegmentIndex) {
//        case 0:
//            [self.mapView removeOverlay:self.diffHeatmap];
//            [self.heatmap setData:[self parseLatLonFile:@"mcdonalds"]];
//            [self.mapView addOverlay:self.heatmap];
//            break;
//        case 1:
//            [self.mapView removeOverlay:self.diffHeatmap];
//            [self.heatmap setData:[self parseLatLonFile:@"first_week"]];
//            [self.mapView addOverlay:self.heatmap];
//            break;
//        case 2:
//            [self.mapView removeOverlay:self.diffHeatmap];
//            [self.heatmap setData:[self parseLatLonFile:@"third_week"]];
//            [self.mapView addOverlay:self.heatmap];
//            break;
//        case 3:
//            [self.mapView removeOverlay:self.heatmap];
//            [self.mapView addOverlay:self.diffHeatmap];
//            break;
//    }
//}

- (IBAction)onLeftBtnPush:(id)sender
{
    NSLog(@"onLeftBtnPush");
//    [[SlideNavigationController sharedInstance] toggleLeftMenu];

//    NSArray *items = @[@"001", @"002", @"003", @"..."];
//    int index;
//    [ActionSheetStringPicker showPickerWithTitle:@"Title"
//                                            rows:items
//                                initialSelection:index
//                                          target:self // コールバックメソッドを保持するクラス
//                                   successAction:@selector(doneSelected:element:) // コールバックメソッド
//                                    cancelAction:@selector(cancellSelected:) origin:sender]; // コールバックメソッド

    NSArray *rows = @[
                      @[@"Hotspa", @"Castle", @"Nightview"],
                      @[@"Japanease", @"English"/*, @"Chinese"*/]
                    ];
    NSArray *initialSelection = @[@0, @0];
    [ActionSheetMultipleStringPicker showPickerWithTitle:@"Select filter"
                                                    rows:rows
                                        initialSelection:initialSelection
                                               doneBlock:^(ActionSheetMultipleStringPicker *picker,
                                                           NSArray *selectedIndexes,
                                                           NSArray *selectedValues) {
                                                   NSLog(@"%@", selectedIndexes);
                                                   NSLog(@"%@", [selectedValues componentsJoinedByString:@", "]);
                                                   NSLog(@"category idx: %@", selectedIndexes[0]);
                                                   NSLog(@"language idx: %@", selectedIndexes[1]);
                                                   
                                                   int categoryIdx = [selectedIndexes[0] intValue];
                                                   int languageIdx = [selectedIndexes[1] intValue];
    
                                                   // api アクセス識別子に変換する
                                                   NSArray *categoryList = @[@"hotspa", @"castle", @"nightview"];
                                                   NSArray *languageList = @[@"ja", @"en"];
                                                   
                                                   mActiveCategory = categoryList[categoryIdx];
                                                   mActiveLanguage = languageList[languageIdx];
                                                   NSLog(@"mActiveCategory: %@", mActiveCategory);
                                                   NSLog(@"mActiveLanguage: %@", mActiveLanguage);
                                                   
                                                   
                                                   
                                                   self.titleLabel.text = mActiveCategory;
                                                   [self reloadHeagmap];
                                               }
                                             cancelBlock:^(ActionSheetMultipleStringPicker *picker) {
                                                 NSLog(@"picker = %@", picker);
                                             } origin:(UIView *)sender];
    
}

- (void)doneSelected:(NSNumber *)selectedIndex element:(id)element {
//    NSLog([NSString stringWithFormat:@"%@",selectedIndex]);
}

- (void)cancellSelected:(id)sender {
//    NSLog(@"Cancel Selected");
}

#pragma mark - MKMapViewDelegate
- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    return [[DTMHeatmapRenderer alloc] initWithOverlay:overlay];
}

- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{
 
    NSLog(@"mapViewDidFinishLoadingMap start");

    // 一定以上の縮尺だったらマーカーを表示する
    // 左上
    CGPoint northWest = CGPointMake(mapView.bounds.origin.x,mapView.bounds.origin.y);
    CLLocationCoordinate2D nwCoord = [mapView convertPoint:northWest toCoordinateFromView:mapView];

    // 右下
    CGPoint southEast = CGPointMake(mapView.bounds.origin.x+mapView.bounds.size.width,mapView.bounds.origin.y+mapView.bounds.size.height);
    CLLocationCoordinate2D seCoord = [mapView convertPoint:southEast toCoordinateFromView:mapView];
    
    double diffLat = nwCoord.latitude - seCoord.latitude;
    double diffLng = seCoord.longitude - nwCoord.longitude;
    NSLog(@"diffLat %f", diffLat);
    NSLog(@"diffLng %f", diffLng);
    
    double THRESHOLD = 3;
    if (diffLng > THRESHOLD && diffLat > THRESHOLD) {
        // ピンを全て削除
        [self.mapView removeAnnotations: _mapView.annotations];
        isMarkerVisible = false;
        return ;
    } else if (isMarkerVisible == true) {
        // 既に表示済みの場合は何もしない
        return ;
    }

    isMarkerVisible = true;
    for (BTHeatData* myHeatData in mHeatmapDataManager.mHeatlist) {
        
        // 表示範囲内のものだけマーカーを追加する(移動を考えるとすべて追加でもよいかも)
        // ピンを全て削除
//        [_mapView removeAnnotations: _mapView.annotations];
        // 新しいピンを作成
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        double lat = [myHeatData.lat doubleValue];
        double lng = [myHeatData.lng doubleValue];
        annotation.coordinate = CLLocationCoordinate2DMake(lat, lng);
        annotation.title = myHeatData.name;
//        annotation.subtitle = @"札幌市中央区北3条西1丁目1-1";
        // ピンを追加
        [self.mapView addAnnotation:annotation];
        
        
    }
    
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    NSLog(@"didSelectAnnotationView start");
    
    // 本来は吹き出しを出してからタップさせるのが理想
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        // 処理内容
//        UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:top] autorelease];

        MQSocialListViewController *socialListViewController = [[MQSocialListViewController alloc] init];
        // 設定
        socialListViewController.searchKeyword = view.annotation.title;
        socialListViewController.activeLocale = mActiveLanguage;
        
        [self presentModalViewController:socialListViewController animated:YES];
//        [self.navigationController pushViewController:socialListViewController animated:YES];


    });
    
    
}

//-(MKAnnotationView*)mapView:(MKMapView*)_mapView viewForAnnotation:(id )annotation {
//    // 現在地表示なら nil を返す
//    if (annotation == mapView.userLocation) {
//        return nil;
//    }
//    
//    MKAnnotationView *annotationView;
//    NSString* identifier = @"Pin";
//    annotationView = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
//    if(nil == annotationView) {
//        annotationView = [[[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier] autorelease];
//    }
//    annotationView.image = [UIImage imageNamed:@"location.png"];
//    annotationView.canShowCallout = YES;
//    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//    
//    annotationView.annotation = annotation;
//    
//    return annotationView;
//}

//#pragma mark - SlideNavigationControllerDelegate
//- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
//{
//    return YES;
//}
//
//- (BOOL)slideNavigationControllerShouldDisplayRightMenu
//{
//    return YES;
//}


@end
