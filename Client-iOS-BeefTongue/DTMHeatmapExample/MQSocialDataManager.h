//
//  MQSocialDataManager.h
//  mahoq
//
//  Created by tana on 2014/11/29.
//  Copyright (c) 2014年 tana. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Accounts/Accounts.h>
#import <Social/Social.h>

#import "STTweetRowData.h"

@interface MQSocialDataManager : NSObject
{

    NSMutableArray *invisTweets; //invisible tweets
    NSMutableArray *visTweets;   //visible tweets
    
    NSDictionary *tweetStatusDic;

    NSString *curLocale;
}

- (void)initialize;
- (void)getTimeline:keyword locale:(NSString *)locale delegate:(id)delegate;
- (STTweetRowData *)setTweetRowDataByTweetDic:(NSDictionary *)curtweetMessage;
-(void)moveInvisTweetsToVisTweets;

@property (nonatomic, retain) NSMutableArray *invisTweets;
@property (nonatomic, retain) NSMutableArray *visTweets;
@property (nonatomic, retain) NSString *curLocale;

@end

@protocol MQSocialDataManagerDelegate
@optional

- (void) refreshTableOnFront;

@end

