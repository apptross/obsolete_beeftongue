//
//  BBEngineClient.h
//  BookButler_iPhone
//
//  Created by tana on 2015/06/22.
//  Copyright (c) 2015 Yuki.Tanaike. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface BBEngineClient : AFHTTPSessionManager

+ (instancetype)sharedInstance;

- (void)loadHeatList:(NSString *)category
            language:(NSString *)language
             handler:(void (^)(NSDictionary *, NSError *))handler;

@end
