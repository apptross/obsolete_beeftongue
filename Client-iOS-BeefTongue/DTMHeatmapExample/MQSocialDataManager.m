//
//  MQSocialDataManager.m
//  mahoq
//
//  Created by tana on 2014/11/29.
//  Copyright (c) 2014年 tana. All rights reserved.
//

#import "MQSocialDataManager.h"
#import "STTweetRowData.h"
#import "SVProgressHUD.h"

#define APP_HASH_TAG        @"#mahoqapp"

//表示メッセージ
#define INFO_STRING_CONNRCTION_ERROR @"Network Error"

@implementation MQSocialDataManager

@synthesize invisTweets;
@synthesize visTweets;
@synthesize curLocale;


- (void)initialize
{
    
    invisTweets = [[NSMutableArray alloc]initWithCapacity:1];
    visTweets = [[NSMutableArray alloc]initWithCapacity:1];

    curLocale = @"ja";
    return ;

}

- (void)getTimeline:keyword locale:(NSString *)locale delegate:(id)delegate
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD show];
    });

    //先にtwitterリストをクリアしておく
    [self cleanTweets];
    [delegate refreshTableOnFront];
    
    //Twitter APIのURLを準備
    //今回は「statuses/home_timeline.json」を利用
    //    NSString *apiURL = @"https://api.twitter.com/1.1/statuses/home_timeline.json";
    NSString *apiURL = @"https://api.twitter.com/1.1/search/tweets.json";
    
    //iOS内に保存されているTwitterのアカウント情報を取得
    ACAccountStore *store = [[ACAccountStore alloc] init];
    ACAccountType *twitterAccountType = [store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    //ユーザーにTwitterの認証情報を使うことを確認
    [store requestAccessToAccountsWithType:twitterAccountType
                                   options:nil
                                completion:^(BOOL granted, NSError *error) {
                                    
                                    
                                    
                                    //ユーザーが拒否した場合
                                    if (!granted) {
                                        
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [SVProgressHUD dismiss];
                                        });
                                        
                                        [self alertAccountProblem];
                                        
                                    } else {//ユーザーの了解が取れた場合
                                        
                                        //デバイスに保存されているTwitterのアカウント情報をすべて取得
                                        NSArray *twitterAccounts = [store accountsWithAccountType:twitterAccountType];
                                        //Twitterのアカウントが1つ以上登録されている場合
                                        if ([twitterAccounts count] > 0) {
                                            //0番目のアカウントを使用
                                            ACAccount *account = [twitterAccounts objectAtIndex:0];
                                            //認証が必要な要求に関する設定
                                            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                                            //[params setObject:@"1" forKey:@"include_entities"];
                                            //                                            [params setObject:@"#nhk" forKey:@"q"];
                                            [params setObject:keyword       forKey:@"q"];
                                            [params setObject:curLocale         forKey:@"locale"];
                                            [params setObject:@"100"        forKey:@"count"];
                                            //リクエストを生成
                                            NSURL *url = [NSURL URLWithString:apiURL];
                                            SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                                                    requestMethod:SLRequestMethodGET
                                                                                              URL:url parameters:params];
                                            //リクエストに認証情報を付加
                                            [request setAccount:account];
                                            //ステータスバーのActivity Indicatorを開始
                                            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                                            
                                            //                                            [SVProgressHUD show];
                                            //リクエストを発行
                                            [request performRequestWithHandler:^(
                                                                                 NSData *responseData,
                                                                                 NSHTTPURLResponse *urlResponse,
                                                                                 NSError *error) {
                                                
                                                //                                                [SVProgressHUD dismiss];
                                                //Twitterからの応答がなかった場合
                                                if (!responseData) {
                                                    
                                                    // inspect the contents of error
                                                    NSLog(@"response error: %@", error);
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                            [SVProgressHUD showErrorWithStatus:INFO_STRING_CONNRCTION_ERROR];
                                                    });
                                                    
                                                } else {//Twitterからの返答があった場合
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        [SVProgressHUD dismiss];
                                                    });
                                                    
                                                    
                                                    //JSONの配列を解析し、TweetをNSArrayの配列に入れる
                                                    NSError *jsonError;
                                                    //                                                    visTweets = [NSJSONSerialization JSONObjectWithData:responseData
                                                    //                                                                                             options: NSJSONReadingMutableLeaves error:&jsonError];
                                                    tweetStatusDic = [NSJSONSerialization JSONObjectWithData: responseData
                                                                                                     options: NSJSONReadingMutableLeaves
                                                                                                       error: &jsonError];
                                                    
                                                    
                                                    invisTweets = [[tweetStatusDic objectForKey:@"statuses"] mutableCopy];
                                                    
                                                    [self moveInvisTweetsToVisTweets];
                                                    
                                                    //Tweet取得完了に伴い、Table Viewを更新
                                                    [delegate refreshTableOnFront];
                                                    
                                                }
                                            }];
                                        } else {
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                [SVProgressHUD dismiss];
                                            });
                                            
                                            [self alertAccountProblem];
                                        }
                                    }
                                    
                                }];
}


////フロント側でテーブルを更新
//- (void) refreshTableOnFront {
//    [self performSelectorOnMainThread:@selector(refreshTable) withObject:self waitUntilDone:TRUE];
//}


//アカウント情報を設定画面で編集するかを確認するalert View表示
-(void)alertAccountProblem {
    // メインスレッドで表示させる
    dispatch_async(dispatch_get_main_queue(), ^{
        //メッセージを表示
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"アカウント"
                              message:@"アカウントに問題があります。「設定」アプリでアカウント情報を確認してください"
                              delegate:self
                              cancelButtonTitle:nil
                              otherButtonTitles:@"はい",
                              nil
                              ];
        [alert show];
    });
}


- (STTweetRowData *)setTweetRowDataByTweetDic:(NSDictionary *)curtweetMessage
{
    STTweetRowData *tweetRowData = [[STTweetRowData alloc]init];
    
    tweetRowData.status = [curtweetMessage objectForKey:@"text"];
    
    NSDictionary *userInfo = [curtweetMessage objectForKey:@"user"];
    tweetRowData.username = [userInfo objectForKey:@"name"];
    
    tweetRowData.profileImgPath = [userInfo objectForKey:@"profile_image_url"];
    
    NSString *twitDateStr = [curtweetMessage objectForKey:@"created_at"];
    //フォーマットを表示用に変換する
    NSDate *curDate = [self twitterCreatedAtToFormatString:twitDateStr];
    tweetRowData.createdAt = curDate;
    
    return tweetRowData;
}

- (NSDate *)twitterCreatedAtToFormatString:(NSString * )dateString{
    //引数をNSDateに変換
    NSDateFormatter *inputFormat = [[NSDateFormatter alloc] init];
    [inputFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [inputFormat setDateFormat:@"eee MMM dd HH:mm:ss ZZZZ yyyy"];
    NSDate *date = [inputFormat dateFromString:dateString];
    return date;
    
}


-(void)moveInvisTweetsToVisTweets
{
    NSLog(@"moveInvisTweetsToVisTweets start");
    NSLog(@"invisTweets.count %lu", (unsigned long)invisTweets.count);
    
    int mvCnt = 10;
    for (int i = 0; i < mvCnt; i++) {
        
        if (invisTweets.count > 0) {
            
            [visTweets addObject:[invisTweets objectAtIndex:0]];
            [invisTweets removeObjectAtIndex:0];
            
        }
        
    }
    
    return ;
    
}

-(void)cleanTweets
{
    
    [invisTweets removeAllObjects];
    [visTweets removeAllObjects];
    
}

@end
