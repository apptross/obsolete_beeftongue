//
//  Util.h
//  BookButler_iPhone
//
//  Created by tanaike on 2013/03/08.
//  Copyright 2013 Yuki Tanaike. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Utility class
 * This class must not have status, so only use class method.(not instanse method)
 */

@interface Util : NSObject
+ (void) dismissSVProgressHUD:(NSString *)successMsg errorMsg:(NSString *)errorMsg;
+ (void) dismissSVProgressHUDWithSuccessMessage:(NSString *)successMsg;
+ (void) dismissSVProgressHUDWithErrorMessage:(NSString *)errorMsg;


@end
