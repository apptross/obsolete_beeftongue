#!/usr/bin/python
#-*- coding: utf-8 -*-
# from config_manager import ConfigManager
import logging
import logging.config
# logging.config.fileConfig(ConfigManager.getLogConfigFilePath())
logging.basicConfig(level=logging.DEBUG)

import time
import urllib
import urllib2
import re
import csv

from translate_data import TranslateData

LIST_FILE_NAME = 'onsen02org.csv'#温泉
# LIST_FILE_NAME = 'shiro02.csv'#城
# LIST_FILE_NAME = '100meizan02.csv'#山
class JpnToEngTranslater:

    # ローカルのテストデータを読みこむ(パース処理のデバッグ用)
    def loadTestData(self, filepath):
        f = open(filepath)
        testData = f.read()
        f.close()

        testData = unicode(testData, 'utf_8')
        return testData

    def loadPageData(self, requestUrl):
        logging.debug("loadPageData start (targetUrl : %s)", requestUrl)

        try:
            req = urllib2.Request(requestUrl)
            response = urllib2.urlopen(req)
            pageData = response.read()

            pageData = unicode(pageData, 'utf_8')

            return pageData

        except urllib2.HTTPError, e:
            logging.debug("[error] wiki page not found")
            return u""

    # 中国語の単語を抽出する
    # エラー発生時は空文字を返す
    def extractTransedChinease(self, pageData):
        logging.debug(" extract transed english word start ")

        searchPreStr = u"title=\"中国語: "
        searchTailStr = u"\" lang=\"zh\" hreflang=\"zh\""
        pattern = searchPreStr + u".+?" + searchTailStr
        m = re.search(pattern, pageData)
        if not m:
            logging.debug(" ERROR :  not found pattern")
            return u""

        extractedSentence = m.group()  # 例：title="英語: Toyotomi Hideyoshi" lang="en" hreflang="en"
        if not extractedSentence:
            logging.debug(" ERROR : extractedSentence is empty")
            return u""

        extractedEnglish = extractedSentence.replace(searchPreStr, "").replace(searchTailStr, "")

        logging.debug(" extractedSentence : %s", extractedSentence)
        logging.debug(" extractedEnglish  : %s", extractedEnglish)

        return extractedEnglish

    # 英語の単語を抽出する
    # エラー発生時は空文字を返す
    def extractTransedEng(self, pageData):
        logging.debug(" extract transed english word start ")

        searchPreStr = u"title=\"英語: "
        searchTailStr = u"\" lang=\"en\" hreflang=\"en\""
        pattern = searchPreStr + u".+?" + searchTailStr
        m = re.search(pattern, pageData)
        if not m:
            logging.debug(" ERROR :  not found pattern")
            return u""

        extractedSentence = m.group()  # 例：title="英語: Toyotomi Hideyoshi" lang="en" hreflang="en"
        if not extractedSentence:
            logging.debug(" ERROR : extractedSentence is empty")
            return u""

        extractedEnglish = extractedSentence.replace(searchPreStr, "").replace(searchTailStr, "")

        logging.debug(" extractedSentence : %s", extractedSentence)
        logging.debug(" extractedEnglish  : %s", extractedEnglish)

        return extractedEnglish

    def generateRequestUrl(self, searchWord):
        searchWord = searchWord.encode('utf-8')
        requestUrl = "https://ja.wikipedia.org/wiki/" + searchWord
        return requestUrl

    def translate(self, org):

        logging.debug(" org jpn  : %s", org)

        requestUrl = self.generateRequestUrl(org)
        loadedPageData = self.loadPageData(requestUrl)
        transedEng = self.extractTransedEng(loadedPageData)

        return self.createTranslateResult(org, transedEng)

    def translateChinease(self, org):

        logging.debug(" org jpn  : %s", org)

        requestUrl = self.generateRequestUrl(org)
        loadedPageData = self.loadPageData(requestUrl)
        transedEng = self.extractTransedChinease(loadedPageData)

        return self.createTranslateResult(org, transedEng)

    def createTranslateResult(self, jpn, eng):

        translateData = TranslateData()
        translateData.eng = eng
        translateData.jpn = jpn

        if eng and jpn:
            translateData.state = TranslateData.TranslatedState
        else:
            translateData.state = TranslateData.NotTranslatedState

        return translateData

    # CSVデータから配列データとして読み込む
    def loadDataAsList(self):
        list = []

        f = open(LIST_FILE_NAME, "r")
        reader = csv.reader(f)
        header = next(reader)

        for row in reader:
            curname = row[1]
            list.append(curname)
            
        f.close()
        return list

    def addResultData(self, str):
         f = open("output.txt", "a")
         f.write(str + '\n')
         f.close()
#         with open('output.txt', mode = 'a', encoding = 'utf-8') as fh:
#             fh.write(str + '\n')
#         return

if __name__ == '__main__':
    logging.debug("main start")

    jpnEngTranslater = JpnToEngTranslater()

    # requestUrl = jpnEngTranslater.generateRequestUrl(u"豊臣秀吉")
    # loadPageData = jpnEngTranslater.loadPageData(requestUrl)
    # logging.debug(" loadPageData : %s", loadPageData)

    # loadPageData = jpnEngTranslater.loadTestData("TestData/TestDataWiki.txt")
    # transedEng = jpnEngTranslater.extractTransedEng(loadPageData)

    nameList = jpnEngTranslater.loadDataAsList()
    for name in nameList:
        logging.debug("name : %s ", name)
        name = unicode(name, 'utf_8')
        
        # 英語翻訳
#         transResult = jpnEngTranslater.translate(name)
        # 中国語翻訳
        transResult = jpnEngTranslater.translateChinease(name)

        logging.debug("transResult jpn : %s eng : %s, state : %d", transResult.jpn, transResult.eng, transResult.state)
        outputStr =  transResult.jpn + u", " + transResult.eng
        jpnEngTranslater.addResultData(outputStr.encode('utf-8'))
        
        #外部サービスへのアクセスのため少しまつ
        time.sleep(2)
    
#     orgJpn = u"豊臣秀吉"
#     transResult = jpnEngTranslater.translate(orgJpn)
#     print transResult
#     jpnEngTranslater.addResultData(transResult)
#     jpnEngTranslater.addResultData(transResult.eng)
    
# 
#     logging.debug("transResult jpn : %s eng : %s, state : %d", transResult.jpn, transResult.eng, transResult.state)
