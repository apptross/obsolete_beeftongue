package com.apptross.beeftongue.wikipediacrawler.main;

import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import com.apptross.beeftongue.common.bean.LandmarkDaoBean;
import com.apptross.beeftongue.common.dao.CategoryDao;
import com.apptross.beeftongue.common.dao.LandmarkDao;
import com.apptross.beeftongue.common.dao.LanguageDao;
import com.apptross.beeftongue.common.dao.MySQLConnector;
import com.apptross.beeftongue.common.dao.WikipediaDao;
import com.apptross.beeftongue.common.exception.DataAccessException;
import com.apptross.beeftongue.common.exception.HttpStatusException;
import com.apptross.beeftongue.common.util.ApptrossLogger;

/**
 * 
 * Wikipediaのアクセス数を取得して、DBに格納するクローラです。
 * 
 * @author Kentaro Ueda
 *
 */
public final class Main
{
	/** ログ */
	private static final Logger log = ApptrossLogger.getLoggerForCallerClass();
	
	/** ヘルプ用のコマンドラインシンタックス */
	private static final String COMMAND_LINE_SYNTAX = "java -jar crawler.jar";
	
	/** language オプション */
	private static final String OPTION_LANGUAGE = "l";
	private static final String OPTION_LANGUAGE_LONG = "language";
	private static final String OPTION_LANGUAGE_DESC = "Specify language";

	/** category オプション */
	private static final String OPTION_CATEGORY = "c";
	private static final String OPTION_CATEGORY_LONG = "category";
	private static final String OPTION_CATEGORY_DESC = "Specify category";
	
	/** category オプション */
	private static final String OPTION_INTERVAL = "i";
	private static final String OPTION_INTERVAL_LONG = "interval";
	private static final String OPTION_INTERVAL_DESC = "Crawl interval in ms";
	
	/**
	 * アプリケーションのメインエントリポイント
	 * 
	 * @param args
	 */
	@SuppressWarnings("static-access")
	public static void main(String[] args)
	{
		Options options = new Options();
		options.addOption(OptionBuilder
				.hasArg(true)
				.isRequired(true)
				.withDescription(OPTION_LANGUAGE_DESC)
				.withLongOpt(OPTION_LANGUAGE_LONG)
				.create(OPTION_LANGUAGE));
		options.addOption(OptionBuilder
				.hasArg(true)
				.isRequired(true)
				.withDescription(OPTION_CATEGORY_DESC)
				.withLongOpt(OPTION_CATEGORY_LONG)
				.create(OPTION_CATEGORY));
		options.addOption(OptionBuilder
				.hasArg(true)
				.isRequired(true)
				.withDescription(OPTION_INTERVAL_DESC)
				.withLongOpt(OPTION_INTERVAL_LONG)
				.create(OPTION_INTERVAL));
		
		CommandLineParser parser = new PosixParser();
		CommandLine cmd = null;
		try
		{
			cmd = parser.parse(options, args);
		}
		catch (ParseException e)
		{
			// パースにエラーが発生した
			// （必須オプションが指定されていないなど）
			HelpFormatter help = new HelpFormatter();
			help.printHelp(COMMAND_LINE_SYNTAX, options);
			return;
		}
		
		// オプションを取得
		String langkey = cmd.getOptionValue(OPTION_LANGUAGE);
		String catkey = cmd.getOptionValue(OPTION_CATEGORY);
		int interval = Integer.valueOf(cmd.getOptionValue(OPTION_INTERVAL));
		
		// データベースへの接続
		com.apptross.beeftongue.common.dao.IDbConnector connector = null;

		try
		{
			// MySQLへ接続します。
			connector = new MySQLConnector();
			
			// 言語idを取得します。
			LanguageDao langdao = new LanguageDao(connector.getConnection());
			int language = langdao.selectLanguageIdByKey(langkey);
			
			// カテゴリidを取得します
			CategoryDao catdao = new CategoryDao(connector.getConnection());
			int category = catdao.selectCategoryIdByKey(catkey);
			
			LandmarkDao lmdao = new LandmarkDao(connector.getConnection());
			List<LandmarkDaoBean> landmarks = lmdao.selectLandmarks(category, language);
			
			for (int i = 0; i < landmarks.size(); i++)
			{
				LandmarkDaoBean lm = landmarks.get(i);
				
				// 名称がnullの時は、クロールを行いません。
				if (lm.getName() == null)
				{
					log.info(ApptrossLogger.replace(ApptrossLogger.I_LANDMARK_NAME_IS_NULL, Integer.toString(lm.getId())));
					lmdao.updateHeat(lm, 0);
					continue;
				}
				else
				{
					WikipediaDao wikidao = new WikipediaDao();
					
					try
					{
						int views = wikidao.calculatePageViews(lm.getName(), langkey);
						lmdao.updateHeat(lm, views);
					}
					catch (HttpStatusException e)
					{
						// Wikipediaから取得できなかったヒートは、0として更新します。
						lmdao.updateHeat(lm, 0);
					}
					catch (DataAccessException e)
					{
						throw e;
					}
				}
				
				Thread.sleep(interval);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
