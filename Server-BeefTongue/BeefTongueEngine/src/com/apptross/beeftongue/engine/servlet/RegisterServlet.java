package com.apptross.beeftongue.engine.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.apptross.beeftongue.common.dao.IDbConnector;
import com.apptross.beeftongue.common.dao.UserDao;
import com.apptross.beeftongue.common.util.ApptrossLogger;
import com.apptross.beeftongue.engine.bean.ApptrossServletBean;
import com.apptross.beeftongue.engine.bean.RegisterServletBean;
import com.apptross.beeftongue.engine.dao.JNDIConnector;

/**
 * Registerリクエストを処理するサーブレットです。
 * 
 * @author Kentaro Ueda
 * 
 */
@WebServlet("/Register")
public final class RegisterServlet extends ApptrossServlet
{
	private static final long serialVersionUID = -5945852256055043297L;

	/** ログ */
	private static final Logger log = ApptrossLogger.getLoggerForCallerClass();
	
	/** ステータス */
	private static final String STATUS_ALREADY_REGISTERED = "registered";
	
	/** パラメータのキー */
	private static final String PARAMETER_KEY_DEVICE_IDENTITY_KEY = "device_identity_key";
	
	/**
	 * コンストラクタです。
	 * @see HttpServlet#HttpServlet()
	 */
	public RegisterServlet()
	{
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		super.doGet(request, response);
		if (!isSustainable())
		{
			return;
		}
		
		// レスポンスの情報をRegisterRequestBeanに確認します。
		RegisterServletBean res = new RegisterServletBean();

		Connection conn = null;
		try
		{
			// パラメータdevice_identity_keyを取得します。
			String devIdKey = getSanitizedParameter(request, PARAMETER_KEY_DEVICE_IDENTITY_KEY);
			
			// デバイス固有のキーが指定されていないとき、エラーステータスを返します。
			if (devIdKey == null)
			{
				log.warning(ApptrossLogger.W_DEVICE_ID_KEY_IS_NULL);
				respond(response, new ApptrossServletBean(STATUS_ERROR));
				return;
			}
			
			// データベースへの接続を取得します。
			IDbConnector connector = new JNDIConnector();
			conn = connector.getConnection();
			UserDao udao = new UserDao(conn);
			
			try
			{
				// すでにユーザが登録されているかどうかを確認します。
				udao.selectUserId(devIdKey);
				
				// 例外がスローされない場合、すでにユーザが登録されています。
				log.warning(ApptrossLogger.W_USER_IS_ALREADY_REGISTERED);
				respond(response, new ApptrossServletBean(STATUS_ALREADY_REGISTERED));
				return;
			}
			catch (Exception e)
			{
				// 正常動作です。
			}

			// ユーザの登録処理を実行します。
			udao.insertUserId(devIdKey);
			
			// ステータスOKを返し、正常終了します。
			res.setStatus(STATUS_OK);
			respond(response, res);
			return;
		}
		catch (Exception e)
		{
			log.warning(e.toString());
		}
		finally
		{
			try
			{
				if (conn != null && !conn.isClosed())	
				{
					conn.close();
				}
			}
			catch (Exception e)
			{
				log.warning(e.toString());
			}
		}
		
		// エラーステータスを返します。
		respond(response, new ApptrossServletBean(STATUS_ERROR));
	}
}
