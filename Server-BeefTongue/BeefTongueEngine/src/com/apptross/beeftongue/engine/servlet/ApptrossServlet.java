package com.apptross.beeftongue.engine.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.apptross.beeftongue.common.exception.InvalidParameterException;
import com.apptross.beeftongue.common.util.ApptrossLogger;
import com.google.gson.Gson;


public abstract class ApptrossServlet extends HttpServlet
{
	private static final long serialVersionUID = 3327081670588211559L;
	
	/** ログ */
	private static final Logger log = ApptrossLogger.getLoggerForCallerClass();

	/** エンコーディング */
	private static final String ENCODING_ISO_8859_1 = "ISO-8859-1";
	private static final String ENCODING_UTF_8 = "UTF-8";
	private static final String CONTENT_TYPE_TEXT_UTF8 = "text/plain; charset=UTF-8";
	
	/** レスポンスに含める共通ステータスです。 */
	protected static final String STATUS_OK = "ok";
	protected static final String STATUS_ERROR = "error";
	protected static final String STATUS_NOT_LOGGED_IN = "notloggedin";
	protected static final String STATUS_NOT_SUPPORTED = "notsupported";
	
	/** セッションオブジェクトのキーです。 */
	protected static final String SESSION_USERID_KEY = "userid";
	
	/** 子クラス側で、処理が続行可能かを設定します。 */ 
	private boolean _sustainable = false;
	
	/** ログインしているユーザのIDです。 */
	private String _userId = null;
	
	/**
	 * @see super{@link #doGet(HttpServletRequest, HttpServletResponse)}
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// リクエストURLを生成します。
		String query = request.getQueryString();
		String completeurl = request.getRequestURL().toString();
		if (query != null)
		{
			completeurl += "?" + query;
		}
		
		// セッションからユーザIDを取得します。
		HttpSession session = request.getSession();
		try
		{
			_userId = (String) session.getAttribute(SESSION_USERID_KEY);
		}
		catch (Exception e)
		{
			// Sessionが無効になっていた場合
			_userId = null;
		}
		
		// リクエストをログ出力します。
		log.info(ApptrossLogger.replace(ApptrossLogger.I_REQUEST, completeurl, _userId));
		
		// 子クラスでの処理を続行します。
		_sustainable = true;
	}
	
	/**
	 * @see super{@link #doPost(HttpServletRequest, HttpServletResponse)}
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// リクエストURLを生成します。
		String query = request.getQueryString();
		String completeurl = request.getRequestURL().toString();
		if (query != null)
		{
			completeurl += "?" + query;
		}
		
		// セッションからユーザIDを取得します。
		HttpSession session = request.getSession();
		try
		{
			_userId = (String) session.getAttribute(SESSION_USERID_KEY);
		}
		catch (Exception e)
		{
			// Sessionが無効になっていた場合
			_userId = null;
		}
		
		// リクエストをログ出力します。
		log.info(ApptrossLogger.replace(ApptrossLogger.I_REQUEST, completeurl, _userId));
		
		// 子クラスでの処理を続行します。
		_sustainable = true;
	}

	/**
	 * 処理が続行可能かを返します。
	 * @return
	 */
	public boolean isSustainable()
	{
		return _sustainable;
	}
	
	/**
	 * ユーザIDを返します。
	 * @return the _userid
	 */
	public String getUserId()
	{
		return _userId;
	}
	
	/**
	 * サーブレットの共通レスポンス処理です。
	 * HTTPステータス200を返し、
	 * BeanをJSONに変換した文字列を、クライアントに返します。
	 * 
	 * @param response
	 * @param bean
	 * @throws IOException 
	 */
	protected void respond(HttpServletResponse response, Object bean)
	{
		// JSONを生成します。
		Gson gson = new Gson();
		String res = gson.toJson(bean);
		respond(response, res);
	}
	
	/**
	 * サーブレットの共通レスポンス処理です。
	 * HTTPステータス200を返し、
	 * 文字列をクライアントに返します。
	 * 
	 * @param response
	 * @param text
	 * @throws IOException 
	 */
	protected void respond(HttpServletResponse response, String text)
	{
		// レスポンスはUTF-8で記述します。
		response.setCharacterEncoding(ENCODING_UTF_8);
		
		// Content-Typeヘッダを付加します。
		response.setContentType(CONTENT_TYPE_TEXT_UTF8);
		
		// レスポンスを返します。
		PrintWriter out = null;
		try
		{
			out = response.getWriter();
			out.println(text);
			out.flush();
			
			// レスポンスをログに出力
			log.info(ApptrossLogger.replace(ApptrossLogger.I_RESPONSE, text));
		}
		catch (Exception e)
		{
			log.warning(e.toString());
		}
		finally
		{
			if (out != null)
			{
				try
				{
					out.close();
				}
				catch (Exception e)
				{
					log.warning(e.toString());
				}
			}
		}
	}
	
	/**
	 * パラメータをサニタイジングして、取得します。
	 * 
	 * @param request
	 * @param key
	 * @param defaultValue
	 * @param pattern
	 * @return
	 * @throws ApplewineServletException
	 */
	protected String getSanitizedParameter(HttpServletRequest request, String key, String defaultValue, String pattern) throws InvalidParameterException
	{
		// 現在のバージョンでは、SQL文生成、JSON生成のライブラリが対応しているため、
		// 特別なエスケープ処理は行いません。
		// UTF-8復号のみ行います。
		String value = unescapeUtf8String(request.getParameter(key));
		
		// valueがnullの場合、デフォルト値を設定します。
		if (value == null)
		{
			value = defaultValue;
		}
		
		// バリデーションを実施します。
		if (pattern != null)
		{
			try
			{
				validateValue(value, pattern);
			}
			catch (InvalidParameterException e)
			{
				throw e;
			}
		}
		
		return value;
	}
	
	/**
	 * @see {@link #getSanitizedParameter(HttpServletRequest, String, String, String)}
	 * @param request
	 * @param key
	 * @return
	 * @throws ApplewineServletException
	 */
	protected String getSanitizedParameter(HttpServletRequest request, String key) throws InvalidParameterException
	{
		return getSanitizedParameter(request, key, null, null);
	}
	
	/**
	 * @see {@link #getSanitizedParameter(HttpServletRequest, String, String, String)}
	 * @param request
	 * @param key
	 * @param defaultValue
	 * @return
	 * @throws ApplewineServletException
	 */
	protected String getSanitizedParameter(HttpServletRequest request, String key, String defaultValue) throws InvalidParameterException
	{
		return getSanitizedParameter(request, key, defaultValue, null);
	}
	
	/**
	 * 値が指定した候補のに含まれているかをバリデートします。
	 * 無効な場合、例外をスローします。
	 * @param value
	 * @param validValues
	 */
	protected void validateValue(String value, String pattern) throws InvalidParameterException
	{
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(value);
		
		if (!m.matches())
		{
			throw new InvalidParameterException();
		}
	}
	
	/**
	 * UTF-8でエスケープされた文字列を変換します。
	 * 
	 * @param source
	 * @return
	 */
	private String unescapeUtf8String(String source) throws InvalidParameterException
	{
		String escstr = null;
		try
		{
			if (source != null)
			{
				escstr = new String(source.getBytes(ENCODING_ISO_8859_1), ENCODING_UTF_8);
			}
		}
		catch (Exception e)
		{
			log.warning(e.toString());
			throw new InvalidParameterException(e);
		}
		
		return escstr;
	}
}
