package com.apptross.beeftongue.engine.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.apptross.beeftongue.common.bean.UserDaoBean;
import com.apptross.beeftongue.common.dao.IDbConnector;
import com.apptross.beeftongue.common.dao.UserDao;
import com.apptross.beeftongue.common.util.ApptrossLogger;
import com.apptross.beeftongue.engine.bean.ApptrossServletBean;
import com.apptross.beeftongue.engine.bean.LoginServletBean;
import com.apptross.beeftongue.engine.dao.JNDIConnector;


/**
 * ログイン処理を行うサーブレットです。
 * 
 * @author Kentaro Ueda
 *
 */
@WebServlet("/Login")
public final class LoginServlet extends ApptrossServlet
{
	private static final long serialVersionUID = -4657181792723283965L;

	/** ログ */
	private static final Logger log = ApptrossLogger.getLoggerForCallerClass();
	
	/** パラメータのキー */
	private static final String PARAMETER_KEY_DEVICE_IDENTITY_KEY = "device_identity_key";
	
	/**
	 * コンストラクタです。
	 */
	public LoginServlet()
	{
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		super.doGet(request, response);
		if (!isSustainable())
		{
			return;
		}
		
		// レスポンスの情報をLoginRequestBeanに格納します。
		LoginServletBean res = new LoginServletBean();
		
		Connection conn = null;
		try
		{
			// パラメータ device_identity_keyを取得します。
			String deviceIdKey = getSanitizedParameter(request, PARAMETER_KEY_DEVICE_IDENTITY_KEY);

			// デバイス固有のキーが指定されていないとき、エラーステータスを返します。
			if (deviceIdKey == null)
			{
				log.warning(ApptrossLogger.W_DEVICE_ID_KEY_IS_NULL);
				respond(response, new ApptrossServletBean(STATUS_ERROR));
				return;
			}
			
			// データベースへの接続を取得します。
			IDbConnector connector = new JNDIConnector();
			conn = connector.getConnection();
			
			// 対象の機種固有のIDから、ユーザIDを取得します。
			UserDao udao = new UserDao(conn);
			UserDaoBean ubean = udao.selectUserId(deviceIdKey);

			// ステータスがアクティブではない場合は、エラーを返します。
			String status = ubean.getStatus();
			if (!status.equals(UserDao.STATUS_ACTIVE))
			{
				log.warning(ApptrossLogger.replace(ApptrossLogger.W_USER_STATUS_IS, status));
				respond(response, new ApptrossServletBean(STATUS_ERROR));
				return;
			}
			
			// ユーザIDをセッションに格納します。
			HttpSession session = request.getSession();
			session.setAttribute(SESSION_USERID_KEY, ubean.getId());
			res.setUserid(ubean.getId());
		
			// ユーザの最終ログイン日時を更新します。
			udao.updateLastLoggedin(ubean.getId());
			
			// ステータスOKを返し、正常終了します。
			res.setStatus(STATUS_OK);
			respond(response, res);
			return;
		}
		catch (Exception e)
		{
			log.warning(e.toString());
		}
		finally
		{
			try
			{
				if (conn != null && !conn.isClosed())	
				{

					conn.close();
				}
			}
			catch (Exception e)
			{
				log.warning(e.toString());
			}
		}
		
		// エラーステータスを返します。
		respond(response, new ApptrossServletBean(STATUS_ERROR));
	}
}
