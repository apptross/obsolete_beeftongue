package com.apptross.beeftongue.engine.servlet;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.apptross.beeftongue.common.util.ApptrossLogger;
import com.apptross.beeftongue.engine.bean.ApptrossServletBean;

/**
 * Logoutリクエストを処理するサーブレットです。
 * 
 * @author Kentaro Ueda
 */
@WebServlet("/Logout")
public final class LogoutServlet extends ApptrossServlet
{
	/** シリアルID */
	private static final long serialVersionUID = -8949115687598466151L;
	
	/** ログ */
	private static final Logger log = ApptrossLogger.getLoggerForCallerClass();
	
	/**
	 * コンストラクタです。
	 */
	public LogoutServlet()
	{
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		super.doGet(request, response);
		if (!this.isSustainable())
		{
			return;
		}
		
		// ログインしていない場合、エラーを返します。
		if (getUserId() == null)
		{
			log.warning(ApptrossLogger.W_USER_ID_IS_NULL);
			respond(response, new ApptrossServletBean(STATUS_NOT_LOGGED_IN));
			return;
		}
		
		try
		{
			// セッション情報を破棄します。
			HttpSession session  = request.getSession();
			session.invalidate();
			
			// 例外がスローされなかったとき、ログアウトに成功したとみなします。
			respond(response, new ApptrossServletBean(STATUS_OK));
			return;
		}
		catch (Exception e)
		{
			log.warning(e.toString());
		}
		
		// エラーステータスを返します。
		respond(response, new ApptrossServletBean(STATUS_ERROR));
	}
}
