package com.apptross.beeftongue.engine.servlet;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.apptross.beeftongue.common.bean.LandmarkDaoBean;
import com.apptross.beeftongue.common.dao.CategoryDao;
import com.apptross.beeftongue.common.dao.IDbConnector;
import com.apptross.beeftongue.common.dao.LandmarkDao;
import com.apptross.beeftongue.common.dao.LanguageDao;
import com.apptross.beeftongue.common.util.ApptrossLogger;
import com.apptross.beeftongue.engine.bean.ApptrossServletBean;
import com.apptross.beeftongue.engine.bean.HeatListServletBean;
import com.apptross.beeftongue.engine.dao.JNDIConnector;

/**
 * ヒートのリストを返すServletです。
 * 
 * @author Kentaro Ueda
 *
 */
@WebServlet("/HeatList")
public final class HeatListServlet extends ApptrossServlet
{
	private static final long serialVersionUID = -3951469884069704917L;

	/** ログ */
	private static final Logger log = ApptrossLogger.getLoggerForCallerClass();
	
	private static final String PARAMETER_KEY_CATEGORY = "category";
	private static final String PARAMETER_KEY_LANGUAGE = "language";
	
	public HeatListServlet()
	{
		super();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		super.doGet(request, response);
		
		HeatListServletBean res = new HeatListServletBean();
		
		Connection conn = null;
		try
		{
			IDbConnector connector = new JNDIConnector();
			conn = connector.getConnection();
			
			String catkey = getSanitizedParameter(request, PARAMETER_KEY_CATEGORY);
			if (catkey == null)
			{
				throw new InvalidParameterException();
			}
			
			CategoryDao catdao = new CategoryDao(conn);
			int catid = catdao.selectCategoryIdByKey(catkey);
			
			String langkey = getSanitizedParameter(request, PARAMETER_KEY_LANGUAGE);
			if (langkey == null)
			{
				throw new InvalidParameterException();
			}
			
			LanguageDao langdao = new LanguageDao(conn);
			int langid = langdao.selectLanguageIdByKey(langkey);
			
			LandmarkDao ld = new LandmarkDao(conn);
			List<LandmarkDaoBean> landmarks = ld.selectLandmarks(catid, langid);
			
			// レスポンスを作成します。
			List<HeatListServletBean.Landmark> reslms = new ArrayList<>();
			for (LandmarkDaoBean daolm : landmarks)
			{
				if (daolm.getName() == null || daolm.getHeat() == 0)
				{
					continue;
				}
				
				HeatListServletBean.Landmark reslm = new HeatListServletBean.Landmark();
				reslm.setId(Integer.toString(daolm.getId()));
				reslm.setName(daolm.getName());
				reslm.setLatitude(Double.toString(daolm.getLatitude()));
				reslm.setLongitude(Double.toString(daolm.getLongitude()));
				reslm.setHeat(Double.toString(daolm.getHeat()));
				
				reslms.add(reslm);
			}
			
			res.setLandmarks(reslms);
			res.setStatus(STATUS_OK);
			respond(response, res);
			return;
		}
		catch (Exception e)
		{
			log.warning(e.toString());
			respond(response, new ApptrossServletBean(STATUS_ERROR));
		}
		finally
		{
			try
			{
				if (conn != null && !conn.isClosed())
				{
					conn.close();
				}
			}
			catch (Exception e)
			{
				log.warning(e.toString());
			}
		}
	}
}
