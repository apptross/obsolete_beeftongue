package com.apptross.beeftongue.engine.dao;

import java.sql.Connection;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.apptross.beeftongue.common.dao.DbConnectorImpl;
import com.apptross.beeftongue.common.exception.DataAccessException;

/**
 * JNDI経由でMySQLと接続し、データベースへの接続を管理するクラスです。
 * 
 * @author Kentaro Ueda
 *
 */
public final class JNDIConnector extends DbConnectorImpl
{
	/** データベース接続先のURIです。 */
	private static final String JDBC_URI = "java:comp/env/jdbc/beeftongue";
	
	/**
	 * JNDIを経由して、データベースへの接続を生成します。
	 * 
	 * @return
	 * @throws Exception
	 */
	protected Connection createConnection() throws DataAccessException
	{
		try
		{
			// JNDI参照コンテキストを取得
			InitialContext initCtx = new InitialContext();
			
			// Tomcatで管理されたデータベース接続をJNDI経由で取得。
			DataSource ds = (DataSource) initCtx.lookup(JDBC_URI);
			Connection conn = ds.getConnection();
			conn.setAutoCommit(false);
			
			return conn;
		}
		catch (Exception e)
		{
			throw new DataAccessException(e);
		}
	}
}
