package com.apptross.beeftongue.engine.bean;

/**
 * 
 * Loginリクエストで使用するJavaBeanです。
 * 
 * @author Kentaro Ueda
 *
 */
public final class LoginServletBean extends ApptrossServletBean
{
	/** ユーザID */
	private String userid;

	/**
	 * @return the userid
	 */
	public String getUserid()
	{
		return userid;
	}

	/**
	 * @param userid the userid to set
	 */
	public void setUserid(String userid)
	{
		this.userid = userid;
	}
}
