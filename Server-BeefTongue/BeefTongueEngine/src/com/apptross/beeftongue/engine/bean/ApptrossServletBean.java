package com.apptross.beeftongue.engine.bean;

/**
 * 
 * ApptrossServletのレスポンス共通のエレメントです。
 * 
 * @author Kentaro Ueda
 *
 */
public class ApptrossServletBean
{
	/** リクエストの実行結果 */
	private String status;
	
	/**
	 * コンストラクタです。
	 */
	public ApptrossServletBean()
	{
		return;
	}
	
	/**
	 * コンストラクタです。
	 * @param status
	 */
	public ApptrossServletBean(String status)
	{
		this.status = status;
	}
	
	/**
	 * @return the status
	 */
	public String getStatus()
	{
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status)
	{
		this.status = status;
	}
}
