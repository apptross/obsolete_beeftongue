package com.apptross.beeftongue.engine.bean;

import java.util.List;

public class HeatListServletBean extends ApptrossServletBean
{
	private List<Landmark> landmarks;
	
	/**
	 * @return the landmark
	 */
	public List<Landmark> getLandmarks()
	{
		return landmarks;
	}

	/**
	 * @param landmark the landmark to set
	 */
	public void setLandmarks(List<Landmark> landmarks)
	{
		this.landmarks = landmarks;
	}

	public static class Landmark
	{
		private String id;
		private String name;
		private String latitude;
		private String longitude;
		private String heat;
		
		/**
		 * @return the id
		 */
		public String getId()
		{
			return id;
		}
		/**
		 * @param id the id to set
		 */
		public void setId(String id)
		{
			this.id = id;
		}
		/**
		 * @return the name
		 */
		public String getName()
		{
			return name;
		}
		/**
		 * @param name the name to set
		 */
		public void setName(String name)
		{
			this.name = name;
		}
		/**
		 * @return the latitude
		 */
		public String getLatitude()
		{
			return latitude;
		}
		/**
		 * @param latitude the latitude to set
		 */
		public void setLatitude(String latitude)
		{
			this.latitude = latitude;
		}
		/**
		 * @return the longitude
		 */
		public String getLongitude()
		{
			return longitude;
		}
		/**
		 * @param longitude the longitude to set
		 */
		public void setLongitude(String longitude)
		{
			this.longitude = longitude;
		}
		/**
		 * @return the heat
		 */
		public String getHeat()
		{
			return heat;
		}
		/**
		 * @param heat the heat to set
		 */
		public void setHeat(String heat)
		{
			this.heat = heat;
		}
	}
}
