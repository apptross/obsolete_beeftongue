package com.apptross.beeftongue.engine.bean;

/**
 * 
 * Registerリクエストで使用するJavaBeanです。
 * 
 * @author Kentaro Ueda
 *
 */
public final class RegisterServletBean extends ApptrossServletBean
{
	// 追加エレメントはありません。
}
