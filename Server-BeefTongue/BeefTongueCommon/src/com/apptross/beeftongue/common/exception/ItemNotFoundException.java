package com.apptross.beeftongue.common.exception;

/**
 * 
 * 検索対象が見つからなかった時にスローされる例外です。
 * 
 * @author Kentaro Ueda
 *
 */
public final class ItemNotFoundException extends DataAccessException
{
	private static final long serialVersionUID = 4487548930168735075L;

	/**
	 * 
	 */
	public ItemNotFoundException()
	{
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ItemNotFoundException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ItemNotFoundException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public ItemNotFoundException(String message)
	{
		super(message);
	}

	/**
	 * @param cause
	 */
	public ItemNotFoundException(Throwable cause)
	{
		super(cause);
	}
}
