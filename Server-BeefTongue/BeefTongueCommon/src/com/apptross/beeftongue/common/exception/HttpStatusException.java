package com.apptross.beeftongue.common.exception;

/**
 * 
 * Httpステータスがエラーとなった場合にスローされる例外です。
 * 
 * @author Kentaro Ueda
 *
 */
public final class HttpStatusException extends DataAccessException
{
	private static final long serialVersionUID = 4784516791971366799L;

	/**
	 * 
	 */
	public HttpStatusException()
	{
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public HttpStatusException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public HttpStatusException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public HttpStatusException(String message)
	{
		super(message);
	}

	/**
	 * @param cause
	 */
	public HttpStatusException(Throwable cause)
	{
		super(cause);
	}
}
