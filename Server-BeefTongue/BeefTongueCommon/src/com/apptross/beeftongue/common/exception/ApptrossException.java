package com.apptross.beeftongue.common.exception;

/**
 * Apptross Templateで使用する例外クラスです。
 * 
 * @author Kentaro Ueda
 *
 */
public class ApptrossException extends Exception
{
	private static final long serialVersionUID = -3566163286787678225L;

	/**
	 * コンストラクタです。
	 */
	public ApptrossException()
	{
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ApptrossException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ApptrossException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public ApptrossException(String message)
	{
		super(message);
	}

	/**
	 * @param cause
	 */
	public ApptrossException(Throwable cause)
	{
		super(cause);
	}
}
