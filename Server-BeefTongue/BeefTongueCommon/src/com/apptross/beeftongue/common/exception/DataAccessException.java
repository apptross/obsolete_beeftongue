package com.apptross.beeftongue.common.exception;

/**
 * データアクセスオブジェクトからスローされる例外です。
 * 
 * @author Kentaro Ueda
 *
 */
public class DataAccessException extends ApptrossException
{
	private static final long serialVersionUID = -4265457562609974041L;

	/**
	 * コンストラクタです。
	 */
	public DataAccessException()
	{
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public DataAccessException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public DataAccessException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public DataAccessException(String message)
	{
		super(message);
	}

	/**
	 * @param cause
	 */
	public DataAccessException(Throwable cause)
	{
		super(cause);
	}
}
