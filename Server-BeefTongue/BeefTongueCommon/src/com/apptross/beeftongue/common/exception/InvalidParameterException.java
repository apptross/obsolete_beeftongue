package com.apptross.beeftongue.common.exception;

/**
 * パラメータ解釈に失敗した時にスローされる例外です。
 * 
 * @author Kentaro Ueda
 *
 */
public final class InvalidParameterException extends ApptrossException
{
	private static final long serialVersionUID = -6685191404057143231L;

	/**
	 * コンストラクタです。
	 */
	public InvalidParameterException()
	{
		super();
	}
	
	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public InvalidParameterException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public InvalidParameterException(String message, Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public InvalidParameterException(String message)
	{
		super(message);
	}

	/**
	 * @param cause
	 */
	public InvalidParameterException(Throwable cause)
	{
		super(cause);
	}
}
