package com.apptross.beeftongue.common.dao;

import java.util.List;

import com.apptross.beeftongue.common.bean.WikipediaDaoBean;
import com.apptross.beeftongue.common.exception.DataAccessException;
import com.google.gson.Gson;

public class WikipediaDao extends ApptrossWebDao
{
	private static final String WIKIPEDIA_ACCESS_ANALYTICS_URL = "https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/%s.wikipedia/all-access/all-agents/%s/daily/2016040100/2016043000";
	private static final String REPLACE_STRING = "%s";
	
	public int calculatePageViews(String query, String lang) throws DataAccessException
	{
		try
		{
			String url = WIKIPEDIA_ACCESS_ANALYTICS_URL.replaceFirst(REPLACE_STRING, lang);
			url = url.replaceFirst(REPLACE_STRING, query);
			
			String restext = httpGetString(url);
			
			Gson g = new Gson();
			WikipediaDaoBean res = g.fromJson(restext, WikipediaDaoBean.class);
			
			List<WikipediaDaoBean.Item> items = res.getItems();
			if (items.size() == 0)
			{
				return 0;
			}
			else
			{
				int viewtotal = 0;
				for (WikipediaDaoBean.Item item : items)
				{
					viewtotal += item.getViews();
				}

				return viewtotal / items.size();
			}
		}
		catch (DataAccessException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new DataAccessException(e);
		}
	}
}
