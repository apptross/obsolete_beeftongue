package com.apptross.beeftongue.common.dao;

import java.sql.Connection;

import com.apptross.beeftongue.common.exception.DataAccessException;

/**
 * データベースに接続し、Connectionを管理するクラスです。
 * 
 * @author Kentaro Ueda
 *
 */
public abstract class DbConnectorImpl implements IDbConnector
{
	/** データソースへの接続です。 */
	protected Connection connection = null;
	
	/* (non-Javadoc)
	 * @see com.fdevided.Apptross.engine.dao.IDbConnector#getConnection()
	 */
	@Override
	public Connection getConnection() throws DataAccessException
	{
		try
		{
			if (connection == null || connection.isClosed())
			{
				connection = createConnection();
			}
		}
		catch (Exception e)
		{
			connection = null;
			throw new DataAccessException(e);
		}

		return connection;
	}
	
	/* (non-Javadoc)
	 * @see com.fdevided.Apptross.engine.dao.IDbConnector#closeConnection()
	 */
	@Override
	public void closeConnection() throws DataAccessException
	{
		try
		{
			connection.close();
		}
		catch (Exception e)
		{
			throw new DataAccessException(e);
		}
		finally
		{
			connection = null;
		}
	}
	
	/**
	 * データベースへの接続を生成します。
	 * 
	 * @return
	 * @throws Exception
	 */
	protected abstract Connection createConnection() throws DataAccessException;
}