package com.apptross.beeftongue.common.dao;

import java.sql.Connection;

import com.apptross.beeftongue.common.exception.DataAccessException;

/**
 * データベースへの接続を管理するインタフェースです。
 * 
 * @author Kentaro Ueda
 *
 */
public interface IDbConnector
{
	/**
	 * データベースへの接続を返します。
	 * 既に接続が確立されている場合、そのインスタンスを返します。
	 * そうでない場合は新規に接続して、そのインスタンスを返します。
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract Connection getConnection() throws DataAccessException;

	/**
	 * 接続を閉じます。
	 */
	public abstract void closeConnection() throws DataAccessException;
}
