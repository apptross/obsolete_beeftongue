package com.apptross.beeftongue.common.dao;

import java.sql.Connection;
import java.sql.DriverManager;

import com.apptross.beeftongue.common.exception.DataAccessException;

/**
 * MySQLと直接接続し、Connectionを管理するクラスです。
 * 
 * @author Kentaro Ueda
 *
 */
public class MySQLConnector extends DbConnectorImpl
{
	/** DBのコネクタクラス名です。 */
	private static final String DRIVER_MYSQL = "com.mysql.jdbc.Driver";
	
	/** DBのURLです。*/
	private static final String LOCAL_DB_URL = "jdbc:mysql://localhost/beeftongue";
	
	/** DBのユーザ名です。 */
	private static final String LOCAL_USERNAME = "beeftongue";
	
	/** DBのパスワードです。 */
	private static final String LOCAL_PASSWORD = "5vqE9diGysl";
	
	/**
	 * コンストラクタです。
	 * @throws Exception
	 */
	public MySQLConnector() throws DataAccessException
	{
		super();
		
		// MySQLドライバを読み込みます。
		try
		{
			Class.forName(DRIVER_MYSQL);
		}
		catch (ClassNotFoundException e)
		{
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * MySQLドライバを経由して、データベースへの接続を生成します。
	 * @return
	 * @throws Exception
	 */
	protected Connection createConnection() throws DataAccessException
	{
		try
		{
			Connection conn = DriverManager.getConnection(LOCAL_DB_URL, LOCAL_USERNAME, LOCAL_PASSWORD);
			conn.setAutoCommit(false);
			
			return conn;
		}
		catch (Exception e)
		{
			throw new DataAccessException(e);
		}
	}
}
