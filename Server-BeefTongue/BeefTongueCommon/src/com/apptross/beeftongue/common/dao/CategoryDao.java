package com.apptross.beeftongue.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

import com.apptross.beeftongue.common.exception.DataAccessException;
import com.apptross.beeftongue.common.exception.ItemNotFoundException;
import com.apptross.beeftongue.common.util.ApptrossLogger;

public class CategoryDao extends ApptrossSqlDao
{
	private static Logger log = ApptrossLogger.getLoggerForCallerClass();
	
	public CategoryDao(Connection connection)
	{
		super(connection);
	}

	public int selectCategoryIdByKey(String key) throws DataAccessException
	{
		final String SQL_SELECT_ID = "SELECT id FROM category WHERE key_name = ?;";
		PreparedStatement stat = null;
		ResultSet rs = null;
		
		try
		{
			stat = getConnection().prepareStatement(SQL_SELECT_ID);
			stat.setString(1, key);
			
			log.info(ApptrossLogger.replace(ApptrossLogger.I_EXECUTE_QUERY, stat));
			rs = stat.executeQuery();
			
			if (rs != null && rs.next())
			{
				return rs.getInt("id");
			}
			else
			{
				throw new ItemNotFoundException();
			}
		}
		catch (DataAccessException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new DataAccessException(e);
		}
		finally
		{
			try
			{
				if (rs != null && !rs.isClosed())
				{
					rs.close();
				}
				
				if (stat != null && !stat.isClosed())
				{
					stat.close();
				}
			}
			catch (Exception e)
			{
				throw new DataAccessException(e);
			}
		}
	}
}
