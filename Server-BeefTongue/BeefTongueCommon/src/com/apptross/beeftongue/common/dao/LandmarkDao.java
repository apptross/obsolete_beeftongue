package com.apptross.beeftongue.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.apptross.beeftongue.common.bean.LandmarkDaoBean;
import com.apptross.beeftongue.common.exception.DataAccessException;
import com.apptross.beeftongue.common.exception.ItemNotFoundException;
import com.apptross.beeftongue.common.util.ApptrossLogger;

/**
 * ヒートに関するテーブルを操作するデータアクセスオブジェクトです。
 * 
 * @author Kentaro Ueda
 *
 */
public final class LandmarkDao extends ApptrossSqlDao
{
	private static Logger log = ApptrossLogger.getLoggerForCallerClass();
	
	public static final int LANG_JA = 1;
	public static final int LANG_EN = 2;
	
	/**
	 * コンストラクタ。
	 * @param connection
	 */
	public LandmarkDao(Connection connection)
	{
		super(connection);
	}

	public String selectLandmarkName(int id, int language) throws DataAccessException
	{
		final String SQL_SELECT_LANDMARK_NAME = "SELECT name FROM landmark_name WHERE landmark = ? AND language = ?";
		PreparedStatement stat = null;
		ResultSet rs = null;
		
		try
		{
			stat = getConnection().prepareStatement(SQL_SELECT_LANDMARK_NAME);
			stat.setInt(1, id);
			stat.setInt(2, language);
			rs = stat.executeQuery();
			
			if (rs != null && rs.next())
			{
				return rs.getString("name");
			}
			else
			{
				throw new ItemNotFoundException();
			}
		}
		catch (DataAccessException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new DataAccessException(e);
		}
		finally
		{
			try
			{
				if (rs != null && !rs.isClosed())
				{
					rs.close();
				}
				
				if (stat != null && !stat.isClosed())
				{
					stat.close();
				}
			}
			catch (Exception e)
			{
				throw new DataAccessException(e);
			}
		}
	}
	
	
	public void updateHeat(LandmarkDaoBean landmark, double heat) throws DataAccessException
	{
		final String SQL_UPDATE_HEAT = "UPDATE heat SET value = ?, modified = now() WHERE landmark = ? AND language = ? ";
		PreparedStatement stat = null;
		
		try
		{
			stat = getConnection().prepareStatement(SQL_UPDATE_HEAT);
			
			stat.setDouble(1, heat);
			stat.setInt(2, landmark.getId());
			stat.setInt(3, landmark.getLanguage());
			
			stat.executeUpdate();
			log.info(ApptrossLogger.replace(ApptrossLogger.I_COMMIT_QUERY, stat));
			getConnection().commit();
			
			return;
		}
		catch (Exception e)
		{
			try
			{
				log.info(ApptrossLogger.replace(ApptrossLogger.I_ROLLBACK_QUERY, stat));
				getConnection().rollback();
			}
			catch (Exception e2)
			{
				throw new DataAccessException(e2);
			}
			
			throw new DataAccessException(e);
		}
		finally
		{
			try
			{
				if (stat != null && !stat.isClosed())
				{
					stat.close();
				}
			}
			catch (Exception e)
			{
				throw new DataAccessException(e);
			}
		}
	}
	
	public void updateHeat(int landmark, int language, double value) throws DataAccessException
	{
		final String SQL_UPDATE_HEAT = "UPDATE heat SET value = ?, modified = now() WHERE landmark = ? AND language = ? ";
		PreparedStatement stat = null;
		
		try
		{
			stat = getConnection().prepareStatement(SQL_UPDATE_HEAT);
			
			stat.setDouble(1, value);
			stat.setInt(2, landmark);
			stat.setInt(3, language);
			
			stat.executeUpdate();
			log.info(ApptrossLogger.replace(ApptrossLogger.I_COMMIT_QUERY, stat));
			getConnection().commit();
			
			return;
		}
		catch (Exception e)
		{
			try
			{
				log.info(ApptrossLogger.replace(ApptrossLogger.I_ROLLBACK_QUERY, stat));
				getConnection().rollback();
			}
			catch (Exception e2)
			{
				throw new DataAccessException(e2);
			}
			
			throw new DataAccessException(e);
		}
		finally
		{
			try
			{
				if (stat != null && !stat.isClosed())
				{
					stat.close();
				}
			}
			catch (Exception e)
			{
				throw new DataAccessException(e);
			}
		}
	}
	
	public List<LandmarkDaoBean> selectLandmarks(int category, int language) throws DataAccessException
	{
		final String SQL_SELECT_LANDMARK = "SELECT landmark.id, landmark.latitude, landmark.longitude, landmark_name.name, heat.value FROM landmark INNER JOIN landmark_name ON landmark.id = landmark_name.landmark INNER JOIN heat ON landmark.id = heat.landmark WHERE landmark.category = ? AND landmark_name.language = ? AND heat.language = ?;";
		
		List<LandmarkDaoBean> landmarks = null;
		PreparedStatement stat = null;
		ResultSet rs = null;
		
		try
		{
			stat = getConnection().prepareStatement(SQL_SELECT_LANDMARK);
			stat.setInt(1, category);
			stat.setInt(2, language);
			stat.setInt(3, language);
			
			log.info(ApptrossLogger.replace(ApptrossLogger.I_EXECUTE_QUERY, stat));
			rs = stat.executeQuery();
			
			if (rs != null)
			{
				landmarks = new ArrayList<LandmarkDaoBean>();
				
				while (rs.next())
				{
					LandmarkDaoBean lm = new LandmarkDaoBean();
					lm.setId(rs.getInt("landmark.id"));
					lm.setName(rs.getString("landmark_name.name"));
					lm.setLatitude(rs.getDouble("landmark.latitude"));
					lm.setLongitude(rs.getDouble("landmark.longitude"));
					lm.setHeat(rs.getDouble("heat.value"));
					lm.setLanguage(language);
					lm.setCategory(category);
					
					landmarks.add(lm);
				}
				
				return landmarks;
			}
			else
			{
				throw new ItemNotFoundException();
			}
		}
		catch (DataAccessException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new DataAccessException(e);
		}
		finally
		{
			try
			{
				if (rs != null && !rs.isClosed())
				{
					rs.close();
				}
				
				if (stat != null && !stat.isClosed())
				{
					stat.close();
				}
			}
			catch (Exception e)
			{
				throw new DataAccessException(e);
			}
		}
	}
	

}
