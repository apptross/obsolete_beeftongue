package com.apptross.beeftongue.common.dao;

import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;

import com.apptross.beeftongue.common.util.ApptrossLogger;

/**
 * データベース接続を扱うデータアクセスオブジェクトです。
 * 
 * @author Kentaro Ueda
 *
 */
public class ApptrossSqlDao
{
	/** ログ */
    @SuppressWarnings("unused")
	private static Logger log = ApptrossLogger.getLoggerForCallerClass();
	
    /** データベースへの接続です。 */ 
	private Connection _connection;
	
	/**
	 * データベースへの接続です。
	 * @return
	 */
	public Connection getConnection()
	{
		return _connection;
	}
	
	/**
	 * コンストラクタです。
	 * @param connection
	 */
	public ApptrossSqlDao(Connection connection)
	{
		_connection = connection;
	}
	
	/**
	 * Date型をCalendar型に変換します。
	 * dateがnullの時は、nullを返します。
	 * 
	 * @param date
	 * @return
	 */
	protected Calendar calendarFromDate(Date date)
	{
		Calendar cal = null;
		
		if (date != null)
		{
			cal = Calendar.getInstance();
			cal.setTime(date);
		}
		
		return cal;
	}
}
