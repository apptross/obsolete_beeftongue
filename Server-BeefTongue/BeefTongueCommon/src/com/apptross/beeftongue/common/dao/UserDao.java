package com.apptross.beeftongue.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.logging.Logger;

import com.apptross.beeftongue.common.bean.UserDaoBean;
import com.apptross.beeftongue.common.exception.ApptrossException;
import com.apptross.beeftongue.common.exception.DataAccessException;
import com.apptross.beeftongue.common.exception.ItemNotFoundException;
import com.apptross.beeftongue.common.util.ApptrossLogger;

/**
 * 
 * ユーザログインに関するデータベーステーブルにアクセスするDAOです。
 * 
 * @author Kentaro Ueda
 *
 */
public final class UserDao extends ApptrossSqlDao
{
	/** ログ */
	private static Logger log = ApptrossLogger.getLoggerForCallerClass();
	
	/** ユーザのステータスです。 */
	public static final String STATUS_ACTIVE = "active";
	public static final String STATUS_INACTIVE = "inactive";
	public static final String STATUS_DELETED = "deleted";

	/**
	 * コンストラクタです。
	 * 
	 * @param connection
	 */
	public UserDao(Connection connection)
	{
		super(connection);
	}
	
	/**
	 * デバイス固有のキーから、ユーザ情報を取得します。
	 * @param deviceIdKey
	 * @return
	 */
	public UserDaoBean selectUserId(String deviceIdKey) throws DataAccessException
	{
		// 初期化
		final String SQL_SELECT_USERS = "select users.id, users.status FROM users WHERE users.device_identity_key = ?;";
		PreparedStatement userStat = null;
		ResultSet userRs = null;
		UserDaoBean user = null;
		
		try
		{
			userStat = this.getConnection().prepareStatement(SQL_SELECT_USERS);
			userStat.setString(1, deviceIdKey);
			
			// クエリ実行
			log.info(ApptrossLogger.replace(ApptrossLogger.I_EXECUTE_QUERY, userStat));
			userRs = userStat.executeQuery();
			
			// 最初の1つを取得結果とします。
			if (userRs != null && userRs.next())
			{
				user = new UserDaoBean();
				user.setId(userRs.getString("users.id"));
				user.setStatus(userRs.getString("users.status"));
			}
			else
			{
				log.warning(ApptrossLogger.replace(ApptrossLogger.W_COULD_NOT_RETRIEVE_USER_ID_BY_DEVKEY, deviceIdKey));
				throw new ItemNotFoundException();
			}
			
			// 正常終了します。
			return user;
		}
		catch (Exception e)
		{
			log.warning(e.toString());
			throw new DataAccessException(e);
		}
		finally
		{
			try
			{
				if (userRs != null && !userRs.isClosed())
				{
					userRs.close();
				}
				
				if (userStat != null && !userStat.isClosed())
				{
					userStat.close();
				}
			}
			catch (Exception e)
			{
				log.warning(e.toString());
				throw new DataAccessException(e);
			}
		}
	}
	
	/**
	 * ユーザの最終ログイン日時を更新します。
	 * @param deviceIdKey
	 * @return
	 */
	public void updateLastLoggedin(String userId) throws DataAccessException
	{
		// 初期化
		final String SQL_UPDATE_LAST_LOGGEDIN = "UPDATE users SET last_loggedin = ? WHERE users.id = ?;";
		PreparedStatement userStat = null;
		
		try
		{
			// Usersテーブルを更新します。
			userStat = this.getConnection().prepareStatement(SQL_UPDATE_LAST_LOGGEDIN);
			
			// 現在の日時を取得します。
			Calendar now = Calendar.getInstance();
			userStat.setTimestamp(1, new Timestamp(now.getTimeInMillis()));
			
			// 検索キーにuserIdを指定します。
			userStat.setString(2, userId);
			
			// usersテーブルに値を挿入します。
			userStat.executeUpdate();
			
			// 例外がスローされなければ、コミットします。
			log.info(ApptrossLogger.replace(ApptrossLogger.I_COMMIT_QUERY, userStat));
			this.getConnection().commit();
			
			// 正常終了します。
			return;
		}
		catch (Exception e)
		{
			log.warning(e.toString());
			
			// ロールバックを実行します。
			try
			{
				log.info(ApptrossLogger.replace(ApptrossLogger.I_ROLLBACK_QUERY, userStat));
				getConnection().rollback();
			}
			catch (Exception e2)
			{
				log.warning(e2.toString());
				throw new DataAccessException(e2);
			}
			
			throw new DataAccessException(e);
		}
		finally
		{
			// クリーンアップ
			try
			{
				if (userStat != null && !userStat.isClosed())
				{
					userStat.close();
				}
			}
			catch (Exception e)
			{
				log.warning(e.toString());
				throw new DataAccessException(e);
			}
		}
	}
	
	/**
	 * デバイス固有のIDを元に、ユーザIDを新規作成します。
	 * 
	 * @param deviceIdKey
	 */
	public UserDaoBean insertUserId(String deviceIdKey) throws ApptrossException
	{
		// 初期化
		final String SQL_INSERT_USERS = "INSERT INTO users (status, device_identity_key, last_loggedin, created, modified) VALUES ('active', ?, now(), now(), now());";
		PreparedStatement userStat = null;
		ResultSet userRs = null;
		UserDaoBean user = null;
		
		try
		{
			// ユーザIDを新規に払い出しします。
			String userId = null;
			userStat = this.getConnection().prepareStatement(SQL_INSERT_USERS, PreparedStatement.RETURN_GENERATED_KEYS);
			userStat.setString(1, deviceIdKey);
			userStat.executeUpdate();
			
			// 払いだされたユーザIDを取得します。
			userRs = userStat.getGeneratedKeys();

			// 自動生成されたidを取得します。
			if (userRs != null && userRs.next())
			{
				userId = Integer.toString(userRs.getInt(1));
			}
			else
			{
				log.warning(ApptrossLogger.replace(ApptrossLogger.W_COULD_NOT_INSERT_INTO_USERS_WITH_DEVKEY, deviceIdKey));
				throw new DataAccessException();
			}

			// ここまで例外がスローされなかった場合、コミットします。
			log.info(ApptrossLogger.replace(ApptrossLogger.I_COMMIT_QUERY, userStat));
			getConnection().commit();
			
			// UserDaoBeanを生成します。
			user = new UserDaoBean();
			user.setId(userId);
			
			// 正常終了します。
			return user;
		}
		catch (Exception e)
		{
			log.warning(e.toString());
			
			// ロールバックを実行します。
			try
			{
				log.info(ApptrossLogger.replace(ApptrossLogger.I_ROLLBACK_QUERY, userStat));
				getConnection().rollback();
			}
			catch (Exception e2)
			{
				log.warning(e2.toString());
				throw new DataAccessException(e2);
			}

			throw new DataAccessException(e);
		}
		finally
		{
			try
			{
				if (userRs != null && !userRs.isClosed())
				{
					userRs.close();
				}
				
				if (userStat != null && !userStat.isClosed())
				{
					userStat.close();
				}
			}
			catch (Exception e)
			{
				log.warning(e.toString());
				throw new DataAccessException(e);
			}
		}
	}
}
