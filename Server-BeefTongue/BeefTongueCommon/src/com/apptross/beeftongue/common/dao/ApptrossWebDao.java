package com.apptross.beeftongue.common.dao;

import java.net.URLEncoder;
import java.util.logging.Logger;

import org.apache.commons.codec.net.URLCodec;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.apptross.beeftongue.common.exception.DataAccessException;
import com.apptross.beeftongue.common.exception.HttpStatusException;
import com.apptross.beeftongue.common.util.ApptrossLogger;

/**
 * 
 * Webアクセス接続を扱うデータアクセスオブジェクトです。
 * 
 * @author Kentaro Ueda
 *
 */
public class ApptrossWebDao
{
	/** ログ */
	private static Logger log = ApptrossLogger.getLoggerForCallerClass();
    
	/** エンコーディング */
	private static final String ENCODING_UTF_8 = "UTF-8";
	
	protected String httpGetString(String url) throws DataAccessException
	{
		HttpClient client = new DefaultHttpClient();
		HttpResponse res = null;
		
		try
		{
			HttpGet req = new HttpGet(url.replace(" ", "%20"));
			log.info(ApptrossLogger.replace(ApptrossLogger.I_HTTP_REQUEST_URL, url));
			res = client.execute(req);
		}
		catch (Exception e)
		{
			throw new DataAccessException(e);
		}
		
		if (res != null)
		{
			int status = res.getStatusLine().getStatusCode();
			log.info(ApptrossLogger.replace(ApptrossLogger.I_HTTP_RESPONSE_STATUS, Integer.toString(status)));
			
			if (status == HttpStatus.SC_OK)
			{
				// 200 OK
				try
				{
					String body = EntityUtils.toString(res.getEntity(), ENCODING_UTF_8);
					log.info(ApptrossLogger.replace(ApptrossLogger.I_HTTP_RESPONSE_BODY, body));
					
					return body;
				}
				catch (Exception e)
				{
					throw new DataAccessException(e);
				}
			}
			else
			{
				// 200 OKではない
				throw new HttpStatusException();
			}
		}
		else
		{
			throw new DataAccessException();
		}
	}
	
}
