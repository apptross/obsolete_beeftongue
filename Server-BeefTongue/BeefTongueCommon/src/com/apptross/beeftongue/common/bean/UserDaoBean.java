package com.apptross.beeftongue.common.bean;

/**
 * 
 * ユーザー情報を格納するJavaBeanです。
 * 
 * @author Kentaro Ueda
 *
 */
public final class UserDaoBean
{
	/** ユーザーIDです。 */
	private String id;
	
	/** ユーザのステータスです。 */
	private String status;
	
	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id)
	{
		this.id = id;
	}

	/**
	 * @return the status
	 */
	public String getStatus()
	{
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status)
	{
		this.status = status;
	}
}
