package com.apptross.beeftongue.common.bean;

import java.util.List;

public final class WikipediaDaoBean
{
	private List<Item> items;
	
	/**
	 * @return the items
	 */
	public List<Item> getItems()
	{
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(List<Item> items)
	{
		this.items = items;
	}

	public static final class Item
	{
		private String project;
		private String article;
		private String granularity;
		private String timestamp;
		private String access;
		private String agent;
		private int views;
		
		/**
		 * @return the project
		 */
		public String getProject()
		{
			return project;
		}
		/**
		 * @param project the project to set
		 */
		public void setProject(String project)
		{
			this.project = project;
		}
		/**
		 * @return the article
		 */
		public String getArticle()
		{
			return article;
		}
		/**
		 * @param article the article to set
		 */
		public void setArticle(String article)
		{
			this.article = article;
		}
		/**
		 * @return the granularity
		 */
		public String getGranularity()
		{
			return granularity;
		}
		/**
		 * @param granularity the granularity to set
		 */
		public void setGranularity(String granularity)
		{
			this.granularity = granularity;
		}
		/**
		 * @return the timestamp
		 */
		public String getTimestamp()
		{
			return timestamp;
		}
		/**
		 * @param timestamp the timestamp to set
		 */
		public void setTimestamp(String timestamp)
		{
			this.timestamp = timestamp;
		}
		/**
		 * @return the access
		 */
		public String getAccess()
		{
			return access;
		}
		/**
		 * @param access the access to set
		 */
		public void setAccess(String access)
		{
			this.access = access;
		}
		/**
		 * @return the agent
		 */
		public String getAgent()
		{
			return agent;
		}
		/**
		 * @param agent the agent to set
		 */
		public void setAgent(String agent)
		{
			this.agent = agent;
		}
		/**
		 * @return the views
		 */
		public int getViews()
		{
			return views;
		}
		/**
		 * @param views the views to set
		 */
		public void setViews(int views)
		{
			this.views = views;
		}
		
	}
}
