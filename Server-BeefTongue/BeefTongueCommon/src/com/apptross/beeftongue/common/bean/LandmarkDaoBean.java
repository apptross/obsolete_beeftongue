package com.apptross.beeftongue.common.bean;

public final class LandmarkDaoBean
{
	private int id;
	private String name;
	private double latitude;
	private double longitude;
	private double heat;
	private int language;
	private int category;
	
	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id)
	{
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	/**
	 * @return the latitude
	 */
	public double getLatitude()
	{
		return latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(double latitude)
	{
		this.latitude = latitude;
	}
	/**
	 * @return the longitude
	 */
	public double getLongitude()
	{
		return longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(double longitude)
	{
		this.longitude = longitude;
	}
	/**
	 * @return the heat
	 */
	public double getHeat()
	{
		return heat;
	}
	/**
	 * @param heat the heat to set
	 */
	public void setHeat(double heat)
	{
		this.heat = heat;
	}
	/**
	 * @return the language
	 */
	public int getLanguage()
	{
		return language;
	}
	/**
	 * @param language the language to set
	 */
	public void setLanguage(int language)
	{
		this.language = language;
	}
	/**
	 * @return the category
	 */
	public int getCategory()
	{
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(int category)
	{
		this.category = category;
	}
}
