package com.apptross.beeftongue.common.util;

import java.sql.PreparedStatement;
import java.util.logging.Logger;

/**
 * Apptrossのログを扱うクラスです。
 * 
 * @author Kentaro Ueda
 *
 */
public class ApptrossLogger
{
	/** ログメッセージの中で、置換可能な文字列 */
	protected static final String REPLACE_STRING = "%s";

	/**
	 * ログに出力するメッセージです。
	 */
	public static final String I_REQUEST = "Request URL: %s, User ID: %s.";
	public static final String I_RESPONSE = "Response Body: %s";
	public static final String I_EXECUTE_QUERY = "Execute query: %s";
	public static final String I_COMMIT_QUERY = "Commit query: %s";
	public static final String I_ROLLBACK_QUERY = "Rollback query: %s";
	public static final String I_HTTP_REQUEST_URL = "Http Request URL: %s";
	public static final String I_HTTP_RESPONSE_STATUS = "Http Response Status: %s";
	public static final String I_HTTP_RESPONSE_BODY = "Http Resonse Body: %s";
	
	public static final String W_USER_ID_IS_NULL = "User ID is null.";
	public static final String W_DEVICE_ID_KEY_IS_NULL = "Device Identity Key is null.";
	public static final String W_USER_IS_NOT_REGISTERED = "User is not registered.";
	public static final String W_USER_STATUS_IS = "User status is %s.";
	public static final String W_USER_IS_ALREADY_REGISTERED = "User is already registered.";
	public static final String W_COULD_NOT_REGISTER_USER = "Could not register user.";
	public static final String W_COULD_NOT_RETRIEVE_USER_ID_BY_DEVKEY = "Could not retrieve User ID by device identity key %s.";
	public static final String W_COULD_NOT_INSERT_INTO_USERS_WITH_DEVKEY = "Could not insert into users with device identity key %s.";
	
	public static final String I_LANDMARK_NAME_IS_NULL = "Landmark name is null. ID: %s";
	
	/**
	 * Loggerクラスを返します。
	 * 
	 * @return Loggerクラスです。
	 */
	public static Logger getLoggerForCallerClass()
	{
		// Loggerクラスが生成されたミリ秒を、固有の識別子とします。
		Long ct = System.currentTimeMillis();
		
		// クラス名を取得します。
		Throwable t = new Throwable();
		StackTraceElement ste = t.getStackTrace()[1];
		
		return Logger.getLogger(ste.getClassName() + Long.toString(ct));
	}
	
	/**
	 * ログのパラメータを置換します。
	 * 
	 * @param msg
	 * @param params
	 * @return
	 */
	public static String replace(String msg, String... params)
	{
		for (String param : params)
		{
			if (param == null)
			{
				msg = msg.replaceFirst(REPLACE_STRING, "NULL");
			}
			else
			{
				msg = msg.replaceFirst(REPLACE_STRING, param);
			}
		}
		
		return msg;
	}
	
	/**
	 * ログのパラメータを置換します。
	 * 
	 * @param msg
	 * @param params
	 * @return
	 */
	public static String replace(String msg, PreparedStatement statement)
	{
		if (statement == null)
		{
			msg = msg.replaceFirst(REPLACE_STRING, "NULL");
		}
		else
		{
			msg = msg.replaceFirst(REPLACE_STRING, statement.toString());
		}
		
		return msg;
	}
}
